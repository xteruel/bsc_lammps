/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author (triclinic) : Pieter in 't Veld (SNL)
------------------------------------------------------------------------- */

#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <algorithm>

#include "comm_brick_ompss.h"
#include "memory.h"
#include "domain.h"
#include "atom_vec.h"
#include "atom.h"

using namespace LAMMPS_NS;

#define BUFFACTOR 1.5
#define BUFMIN 1000
#define BUFEXTRA 1000
#define BIG 1.0e20

#define COPY3( a, b ) { a[0] = b[0]; a[1] = b[1]; a[2] = b[2]; }

enum{SINGLE,MULTI};               // same as in Comm

/* ---------------------------------------------------------------------- */

CommBrickOMPSS::CommBrickOMPSS(LAMMPS *lmp) :
  CommBrick(lmp)
{
  pbuf_sr_list      = NULL;
  pbuf_sr_list_max  = NULL;
  pbuf_sr_list_size = NULL;
  prequest          = NULL;
  pmax_send         = 0;
  pmax_recv         = 0;

  init_buffers();
}

/* ---------------------------------------------------------------------- */

CommBrickOMPSS::CommBrickOMPSS( CommBrick *oldcomm ) :
  CommBrick(oldcomm)
{
  pbuf_sr_list      = NULL;
  pbuf_sr_list_max  = NULL;
  pbuf_sr_list_size = NULL;
  prequest          = NULL;
  pmax_send         = 0;
  pmax_recv         = 0;

  init_buffers();
}

/* ---------------------------------------------------------------------- */

CommBrickOMPSS::~CommBrickOMPSS()
{
  if (pbuf_sr_list_size) memory->destroy(pbuf_sr_list_size);
  if (pbuf_sr_list_max) memory->destroy(pbuf_sr_list_max);
  if (pbuf_sr_list) {
    for (int i= 0; i< nworkers; i++ )
      if (pbuf_sr_list[i])
        memory->destroy( pbuf_sr_list[i] );
     memory->sfree( pbuf_sr_list );
  }
  if (prequest) delete [] prequest;
}

void CommBrickOMPSS::init_buffers()
{
  pbuf_sr_list = (int **) memory->smalloc(nworkers*sizeof(int *),"comm:pbuf_sr_list");
  memory->create( pbuf_sr_list_max, nworkers, "comm:pbuf_sr_list_max" );
  memory->create( pbuf_sr_list_size, nworkers, "comm:pbuf_sr_list_size" );
  int sr_list_max = 0;
  for (int i= 0; i< maxswap; i++ )
    if (sr_list_max< maxsendlist[i] )
      sr_list_max = maxsendlist[i];
  sr_list_max = static_cast<int> (BUFFACTOR * (sr_list_max/nworkers));
  if (sr_list_max==0) sr_list_max = BUFMIN;
  for (int i= 0; i< nworkers; i++ ) {
    pbuf_sr_list_max[i] = sr_list_max;
    memory->create( pbuf_sr_list[i], sr_list_max, "comm:pbuf_sr_list[i]" );
  }
  prequest = new MPI_Request[maxswap];
}


/* ----------------------------------------------------------------------
   reverse communication of forces on atoms every timestep
   other per-atom attributes may also be sent via pack/unpack routines
------------------------------------------------------------------------- */

void CommBrickOMPSS::reverse_comm()
{
# if 1 == 1
  int n;
  MPI_Request request;
  AtomVec *avec = atom->avec;
  double **f = atom->f;
  double *buf;

  // exchange data with another proc
  // if other proc is self, just copy
  // if comm_f_only set, exchange or copy directly from f, don't pack

  for (int iswap = nswap-1; iswap >= 0; iswap--) {
    if (sendproc[iswap] != me) {
      if (comm_f_only) {
        if (size_reverse_recv[iswap])
          MPI_Irecv(buf_recv,size_reverse_recv[iswap],MPI_DOUBLE,
                    sendproc[iswap],0,world,&request);
        if (size_reverse_send[iswap]) {
          buf = f[firstrecv[iswap]];
          MPI_Send(buf,size_reverse_send[iswap],MPI_DOUBLE,
                   recvproc[iswap],0,world);
        }
        if (size_reverse_recv[iswap]) MPI_Wait(&request,MPI_STATUS_IGNORE);
      } else {
        if (size_reverse_recv[iswap])
          MPI_Irecv(buf_recv,size_reverse_recv[iswap],MPI_DOUBLE,
                    sendproc[iswap],0,world,&request);
        n = avec->pack_reverse(recvnum[iswap],firstrecv[iswap],buf_send);
        if (n) MPI_Send(buf_send,n,MPI_DOUBLE,recvproc[iswap],0,world);
        if (size_reverse_recv[iswap]) MPI_Wait(&request,MPI_STATUS_IGNORE);
      }
      avec->unpack_reverse(sendnum[iswap],sendlist[iswap],buf_recv);

    } else {
      if (comm_f_only) {
        if (sendnum[iswap])
          avec->unpack_reverse(sendnum[iswap],sendlist[iswap],
                               f[firstrecv[iswap]]);
      } else {
        avec->pack_reverse(recvnum[iswap],firstrecv[iswap],buf_send);
        avec->unpack_reverse(sendnum[iswap],sendlist[iswap],buf_send);
      }
    }
  }
#endif

#if 0
  if (pmax_recv==0) {
    pmax_recv = maxrecv;
    grow_recv(pmax_recv*nswap);
  }
  if (pmax_send==0) {
    pmax_send = maxsend;
    grow_recv(pmax_send*nswap);
  }

  // exchange data with another proc
  // if other proc is self, just copy
  // if comm_f_only set, exchange or copy directly from f, don't pack

  AtomVec *avec = atom->avec;
  double **f = atom->f;
# pragma omp parallel default(none) shared(avec,f)
# pragma omp single
  {
    for (int iswap = 0; iswap < nswap; iswap++) {
      if (sendproc[iswap] != me) {
        if (size_reverse_send[iswap]) {
#         pragma omp task firstprivate(iswap)
          {
            double *buf;
            if (comm_f_only) {
              buf = f[firstrecv[iswap]];
            } else {
              buf = buf_send + iswap*pmax_send;
          	  int n = avec->pack_reverse(recvnum[iswap],firstrecv[iswap],buf);
            }
            MPI_Isend(buf,size_reverse_send[iswap],MPI_DOUBLE,
                     recvproc[iswap],iswap,world,&prequest[iswap]);
          }
        }
        if (size_reverse_recv[iswap]) {
#         pragma omp task firstprivate(iswap)
          {
            double *buf = buf_recv + iswap*pmax_recv;
            MPI_Status status;
            MPI_Recv(buf,size_reverse_recv[iswap],MPI_DOUBLE,
                      sendproc[iswap],iswap,world,&status);
#           pragma omp critical
            avec->unpack_reverse(sendnum[iswap],sendlist[iswap],buf);
          }
        }
      } else {
        if (comm_f_only) {
          if (sendnum[iswap]) {
#           pragma omp task firstprivate(iswap)
            {
#             pragma omp critical
              avec->unpack_reverse(sendnum[iswap],sendlist[iswap],
                                   f[firstrecv[iswap]]);
            }
          }
        } else {
          double *buf = buf_send + iswap*pmax_send;
#         pragma omp task firstprivate(iswap,buf) depend(out:buf)
          avec->pack_reverse(recvnum[iswap],firstrecv[iswap],buf);
#         pragma omp task firstprivate(iswap,buf) depend(in:buf)
#         pragma omp critical
          avec->unpack_reverse(sendnum[iswap],sendlist[iswap],buf);
        }
      }
    }
  }

#endif

# if 0
  MPI_Request *request = new MPI_Request[nswap];
  int *ilist = new int[nswap];
  AtomVec *avec = atom->avec;
  double **f = atom->f;

  int nr = 0;
  for (int iswap = nswap-1; iswap >= 0; iswap--) {
    if (sendproc[iswap] != me) {
      if (size_reverse_recv[iswap]) {
#       if _OMPSS == 2
#       pragma oss task firstprivate(nr,iswap) out(request[nr])
#       endif
        {
          MPI_Irecv(buf_recv+iswap*pmax_recv,size_reverse_recv[iswap],MPI_DOUBLE,
                  sendproc[iswap],iswap,world,&request[nr]);
        }
        ilist[nr] = iswap;
        nr++;
      }
      if (size_reverse_send[iswap]) {
#       if _OMPSS == 2
#       pragma oss task firstprivate(iswap)
#       endif
        {
        double *buf = f[firstrecv[iswap]];
        MPI_Send(buf,size_reverse_send[iswap],MPI_DOUBLE,
                 recvproc[iswap],iswap,world);
        }
      }

    } else {
#   if _OMPSS == 2
# 	pragma oss task commutative(f)
#   endif
      {
        if (comm_f_only) {
          if (sendnum[iswap])
            avec->unpack_reverse(sendnum[iswap],sendlist[iswap],
                                 f[firstrecv[iswap]]);
        } else {
          avec->pack_reverse(recvnum[iswap],firstrecv[iswap],buf_send);
          avec->unpack_reverse(sendnum[iswap],sendlist[iswap],buf_send);
        }
      }
    }
  }
  for (int i=0; i< nr; i++) {
#   if _OMPSS == 2
# 	pragma oss task out(request[i])
#   endif
    {
      MPI_Wait(&request[i],MPI_STATUS_IGNORE);
    }
#   if _OMPSS == 2
# 	pragma oss task in(request[i]) commutative(f)
#   endif
    {
      int iswap = ilist[i];
      avec->unpack_reverse(sendnum[iswap],sendlist[iswap],buf_recv+iswap*pmax_recv);
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  delete [] ilist;
  delete [] request;
# endif
}



void CommBrickOMPSS::exchange()
{
  int m,nsend,nrecv,nrecv1,nrecv2;
  double value;
  double *sublo,*subhi;
  MPI_Request request;
  AtomVec *avec = atom->avec;

  // clear global->local map for owned and ghost atoms
  // b/c atoms migrate to new procs in exchange() and
  //   new ghosts are created in borders()
  // map_set() is done at end of borders()
  // clear ghost count and any ghost bonus data internal to AtomVec

  if (map_style) atom->map_clear();

  atom->nghost = 0;
  atom->avec->clear_bonus();

  // insure send buf is large enough for single atom
  // bufextra = max size of one atom = allowed overflow of sendbuf
  // fixes can change per-atom size requirement on-the-fly

  int bufextra_old = bufextra;
  maxexchange = maxexchange_atom + maxexchange_fix;
  bufextra = maxexchange + BUFEXTRA;
  if (bufextra > bufextra_old)
    memory->grow(buf_send,maxsend+bufextra,"comm:buf_send");

  // subbox bounds for orthogonal or triclinic

  if (triclinic == 0) {
    sublo = domain->sublo;
    subhi = domain->subhi;
  } else {
    sublo = domain->sublo_lamda;
    subhi = domain->subhi_lamda;
  }

  // loop over dimensions

  int dimension = domain->dimension;

  for (int dim = 0; dim < dimension; dim++) {
    // fill buffer with atoms leaving my box, using < and >=
    // when atom is deleted, fill it in with last atom
    double lo = sublo[dim];
    double hi = subhi[dim];
# 	if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for default(none) \
    shared(dim,lo,hi)
# 	elif   _OMPSS == 2
#   pragma oss task for default(none) \
    shared(dim,lo,hi)
#   endif
    for (int t=0; t< nworkers; t++) {
      int th_npack = 0;
      double **x = atom->x;
      int nlocal = atom->nlocal;
      int di     = nlocal/nworkers;
      int mo     = nlocal%nworkers;
      int ini    = t*di + ( t < mo ? t : mo );
      int end    = ini + di + ( t < mo ? 1 : 0 );
      for (int i=ini; i< end; i++) {
        if (x[i][dim] < lo || x[i][dim] >= hi) {
          if (th_npack == pbuf_sr_list_max[t]) grow_pbuf_sr_list(t,th_npack);
          pbuf_sr_list[t][th_npack++] = i;
        }
      }
      pbuf_sr_list_size[t] = th_npack;
    }
# if _OMPSS == 2
# pragma oss taskwait
# endif

    int nlocal = atom->nlocal;
    int nsend  = 0;
    int lt     = nworkers-1;
    while(lt>=0 && pbuf_sr_list_size[lt]==0) lt--;
    int last = lt>= 0 ? pbuf_sr_list[lt][pbuf_sr_list_size[lt]-1] : -1;
    
    for (int t=0; t< nworkers; t++) {
      for (int ii=0; ii< pbuf_sr_list_size[t]; ii++) {
        int i = pbuf_sr_list[t][ii];
        if (nsend > maxsend) grow_send(nsend,1);
        nsend += avec->pack_exchange(i,&buf_send[nsend]);
        nlocal--;
        while (last==nlocal && last>i) {
          if (nsend > maxsend) grow_send(nsend,1);
          nsend += avec->pack_exchange(last,&buf_send[nsend]);
          nlocal--;
          // Update last
          pbuf_sr_list_size[lt] -= 1;
          while(lt>=0 && pbuf_sr_list_size[lt]==0) lt--;
          last = lt>= 0 ? pbuf_sr_list[lt][pbuf_sr_list_size[lt]-1] : -1;
        }
        if (i<nlocal) avec->copy(nlocal,i,1);
      }
    }

    atom->nlocal = nlocal;
    // send/recv atoms in both directions
    // send size of message first so receiver can realloc buf_recv if needed
    // if 1 proc in dimension, no send/recv
    //   set nrecv = 0 so buf_send atoms will be lost
    // if 2 procs in dimension, single send/recv
    // if more than 2 procs in dimension, send/recv to both neighbors
    if (procgrid[dim] == 1) nrecv = 0;
    else {
      MPI_Sendrecv(&nsend,1,MPI_INT,procneigh[dim][0],0,
                   &nrecv1,1,MPI_INT,procneigh[dim][1],0,world,
                   MPI_STATUS_IGNORE);
      nrecv = nrecv1;
      if (procgrid[dim] > 2) {
        MPI_Sendrecv(&nsend,1,MPI_INT,procneigh[dim][1],0,
                     &nrecv2,1,MPI_INT,procneigh[dim][0],0,world,
                     MPI_STATUS_IGNORE);
        nrecv += nrecv2;
      }
      if (nrecv > maxrecv) grow_recv(nrecv);

      MPI_Irecv(buf_recv,nrecv1,MPI_DOUBLE,procneigh[dim][1],0,
                world,&request);
      MPI_Send(buf_send,nsend,MPI_DOUBLE,procneigh[dim][0],0,world);
      MPI_Wait(&request,MPI_STATUS_IGNORE);

      if (procgrid[dim] > 2) {
        MPI_Irecv(&buf_recv[nrecv1],nrecv2,MPI_DOUBLE,procneigh[dim][0],0,
                  world,&request);
        MPI_Send(buf_send,nsend,MPI_DOUBLE,procneigh[dim][1],0,world);
        MPI_Wait(&request,MPI_STATUS_IGNORE);
      }
    }

    // check incoming atoms to see if they are in my box
    // if so, add to my list
    // box check is only for this dimension,
    //   atom may be passed to another proc in later dims

    m = 0;
    while (m < nrecv) {
      value = buf_recv[m+dim+1];
      if (value >= lo && value < hi) m += avec->unpack_exchange(&buf_recv[m]);
      else m += static_cast<int> (buf_recv[m]);
    }
  }
  if (atom->firstgroupname) atom->first_reorder();
}

void CommBrickOMPSS::borders()
{
  int         nsend, nrecv, sendflag, nfirst, nlast, n;
  double      lo, hi;
  int        *type;
  double     *buf,*mlo,*mhi;
  MPI_Request request;
  AtomVec    *avec = atom->avec;

  // do swaps over all 3 dimensions

  int iswap = 0;
  smax = rmax = 0;

  for (int dim = 0; dim < 3; dim++) {
    nlast = 0;
    int twoneed = 2*maxneed[dim];
    for (int ineed = 0; ineed < twoneed; ineed++) {

      // find atoms within slab boundaries lo/hi using <= and >=
      // check atoms between nfirst and nlast
      //   for first swaps in a dim, check owned and ghost
      //   for later swaps in a dim, only check newly arrived ghosts
      // store sent atom indices in sendlist for use in future timesteps

      double **x = atom->x;
      if (mode == SINGLE) {
        lo = slablo[iswap];
        hi = slabhi[iswap];
      } else {
        type = atom->type;
        mlo = multilo[iswap];
        mhi = multihi[iswap];
      }
      if (ineed % 2 == 0) {
        nfirst = nlast;
        nlast = atom->nlocal + atom->nghost;
      }

      nsend = 0;

      // sendflag = 0 if I do not send on this swap
      // sendneed test indicates receiver no longer requires data
      // e.g. due to non-PBC or non-uniform sub-domains

      if (ineed/2 >= sendneed[dim][ineed % 2]) sendflag = 0;
      else sendflag = 1;

      // find send atoms according to SINGLE vs MULTI
      // all atoms eligible versus only atoms in bordergroup
      // can only limit loop to bordergroup for first sends (ineed < 2)
      // on these sends, break loop in two: owned (in group) and ghost

      if (sendflag) {
#       if _OMPSS == 1 || defined(_OPENMP)
#       pragma omp parallel for default(none) \
        shared(nfirst,nlast,dim,x,lo,mlo,hi,mhi,ineed,type)
#       elif _OMPSS == 2
#       pragma oss task for default(none) \
        shared(nfirst,nlast,dim,x,lo,mlo,hi,mhi,ineed,type)
#       endif
        for (int t=0; t< nworkers; t++) {
          int th_nsend = 0;
          if (!bordergroup || ineed >= 2) {
            int n        = nlast - nfirst;
            int di       = n/nworkers;
            int mo       = n%nworkers;
            int ini      = nfirst + t*di + ( t < mo ? t : mo );
            int end      = ini + di + ( t < mo ? 1 : 0 );

            if (mode == SINGLE) {
              for (int i = ini; i < end; i++)
                if (x[i][dim] >= lo && x[i][dim] <= hi) {
                  if (th_nsend == pbuf_sr_list_max[t]) grow_pbuf_sr_list(t,th_nsend);
                  pbuf_sr_list[t][th_nsend++] = i;
                }
            } else {
              for (int i = ini; i < end; i++) {
                int itype = type[i];
                if (x[i][dim] >= mlo[itype] && x[i][dim] <= mhi[itype]) {
                  if (th_nsend == pbuf_sr_list_max[t]) grow_pbuf_sr_list(t,th_nsend);
                  pbuf_sr_list[t][th_nsend++] = i;
                }
              }
            }
          } else {
            int ngroup   = atom->nfirst;
            int ndiff    = atom->nlocal - ngroup;
            int  n       = ngroup + nlast - atom->nlocal;
            int di       = n/nworkers;
            int mo       = n%nworkers;
            int ini      = t*di + ( t < mo ? t : mo );
            int end      = ini + di + ( t < mo ? 1 : 0 );

            if (mode == SINGLE) {
              for (int ii = ini; ii < end; ii++) {
                int i = ii < atom->nfirst ? ii : ii+ndiff;
                if (x[i][dim] >= lo && x[i][dim] <= hi) {
                  if (th_nsend == pbuf_sr_list_max[t]) grow_pbuf_sr_list(t,th_nsend);
                  pbuf_sr_list[t][th_nsend++] = i;
                }
              }
            } else {
              for (int ii = ini; ii < end; ii++) {
                int i = ii < atom->nfirst ? ii : ii+ndiff;
                int itype = type[i];
                if (x[i][dim] >= mlo[itype] && x[i][dim] <= mhi[itype]) {
                  if (th_nsend == pbuf_sr_list_max[t]) grow_pbuf_sr_list(t,th_nsend);
                  pbuf_sr_list[t][th_nsend++] = i;
                }
              }
            }
          }
          pbuf_sr_list_size[t] = th_nsend;
        }
#       if _OMPSS == 2
#       pragma oss taskwait
#       endif
        for (int t=0; t< nworkers; t++) nsend += pbuf_sr_list_size[t];
        if (nsend > maxsendlist[iswap]) grow_list(iswap,nsend);
        
        nsend = 0;
        for (int t=0; t< nworkers; t++) {
          memcpy( sendlist[iswap] + nsend, pbuf_sr_list[t], pbuf_sr_list_size[t]*sizeof(int) );
          nsend += pbuf_sr_list_size[t];
        }
      }

      // pack up list of border atoms

      if (nsend*size_border > maxsend) grow_send(nsend*size_border,0);
      if (ghost_velocity)
        n = avec->pack_border_vel(nsend,sendlist[iswap],buf_send,
                                  pbc_flag[iswap],pbc[iswap]);
      else
        n = avec->pack_border(nsend,sendlist[iswap],buf_send,
                              pbc_flag[iswap],pbc[iswap]);

      // swap atoms with other proc
      // no MPI calls except SendRecv if nsend/nrecv = 0
      // put incoming ghosts at end of my atom arrays
      // if swapping with self, simply copy, no messages

      if (sendproc[iswap] != me) {
        MPI_Sendrecv(&nsend,1,MPI_INT,sendproc[iswap],0,
                     &nrecv,1,MPI_INT,recvproc[iswap],0,world,
                     MPI_STATUS_IGNORE);
        if (nrecv*size_border > maxrecv) grow_recv(nrecv*size_border);
        if (nrecv) MPI_Irecv(buf_recv,nrecv*size_border,MPI_DOUBLE,
                             recvproc[iswap],0,world,&request);
        if (n) MPI_Send(buf_send,n,MPI_DOUBLE,sendproc[iswap],0,world);
        if (nrecv) MPI_Wait(&request,MPI_STATUS_IGNORE);
        buf = buf_recv;
      } else {
        nrecv = nsend;
        buf = buf_send;
      }

      // unpack buffer

      if (ghost_velocity)
        avec->unpack_border_vel(nrecv,atom->nlocal+atom->nghost,buf);
      else
        avec->unpack_border(nrecv,atom->nlocal+atom->nghost,buf);

      // set all pointers & counters

      smax = MAX(smax,nsend);
      rmax = MAX(rmax,nrecv);
      sendnum[iswap] = nsend;
      recvnum[iswap] = nrecv;
      size_forward_recv[iswap] = nrecv*size_forward;
      size_reverse_send[iswap] = nrecv*size_reverse;
      size_reverse_recv[iswap] = nsend*size_reverse;
      firstrecv[iswap] = atom->nlocal + atom->nghost;
      atom->nghost += nrecv;
      iswap++;
    }
  }
  // insure send/recv buffers are long enough for all forward & reverse comm

  pmax_send = MAX(maxforward*smax,maxreverse*rmax);
  if (pmax_send*nswap > maxsend) grow_send(pmax_send*nswap,0);
  pmax_recv = MAX(maxforward*rmax,maxreverse*smax);
  if (pmax_recv*nswap > maxrecv) grow_recv(pmax_recv*nswap);

  // reset global->local map

  if (map_style) atom->map_set();
}


/* ----------------------------------------------------------------------
   realloc the size of the i-th pbuf_sr_list as needed with BUFFACTOR
------------------------------------------------------------------------- */

void CommBrickOMPSS::grow_pbuf_sr_list(int i, int n)
{
  pbuf_sr_list_max[i] = static_cast<int> (BUFFACTOR * n);
  memory->grow(pbuf_sr_list[i],pbuf_sr_list_max[i],"comm:pbuf_sr_list[i]");
}

