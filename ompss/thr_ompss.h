/* -------------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef LMP_THR_OMPSS_H
#define LMP_THR_OMPSS_H

#include "pointers.h"
#include "fix_ompss.h"
#include <stdlib.h>

// FFT_PRECISION = 1 is single-precision complex (4-byte real, 4-byte imag)
// FFT_PRECISION = 2 is double-precision complex (8-byte real, 8-byte imag)

#ifdef FFT_SINGLE
#define FFT_PRECISION 1
typedef float FFT_SCALAR;
#else
#define FFT_PRECISION 2
typedef double FFT_SCALAR;
#endif

namespace LAMMPS_NS {

// forward declarations
class FixOMPSS;
class Pair;
class Bond;
class Angle;
class Dihedral;
class Improper;
class Fix;
class KSpace;

class ThrOMPSS {
 public:
  ThrOMPSS( LAMMPS * );
  ThrOMPSS( ) { };

  template <class T> void init( T *, int );
  void init( Fix *, int );

  void ev_tally_thr( Pair * const, const int, const int, const int, const int,
                     const double, const double, const double,
                     const double, const double, const double );
  void ev_tally_thr( Bond * const, const int, const int, const int, const int,
                     const double, const double,
                     const double, const double, const double );
  void ev_tally_thr( Angle * const, const int, const int, const int, const int, const int,
                     const double, const double * const, const double * const,
                     const double, const double, const double, const double, const double,
                     const double );
  void ev_tally_thr( Dihedral * const, const int, const int, const int, const int, const int,
                     const int, const double, const double * const, const double * const,
                     const double * const, const double, const double, const double,
                     const double, const double, const double, const double, const double,
                     const double );
  void ev_tally_thr( Improper * const, const int, const int, const int, const int, const int,
                     const int, const double, const double * const, const double * const,
                     const double * const, const double, const double, const double,
                     const double, const double, const double, const double, const double,
                     const double);

  void ev_tally_xyz_thr( Pair * const, const int, const int, const int,
                         const int, const double, const double, const double,
                         const double, const double, const double,
                         const double, const double );


  void e_tally_thr( Pair * const, const int, const int, const int, const int ,
                    const double, const double );
  void e_tally_thr( Bond * const, const int, const int, const int, const int ,
                    const double );

  template <class T>
  void v_tally_thr( T * const, const int, const int, const int, const int, const double * const );

  void v_tally_thr( int, int, int, int *, double, double * );


  void reduce_thr( Pair *, const int, const int );
  void reduce_thr( Bond *, const int, const int );
  void reduce_thr( Angle *, const int, const int );
  void reduce_thr( Dihedral *, const int, const int );
  void reduce_thr( Improper *, const int, const int );
  void reduce_thr( Fix *, const int );

  void reduce_arrays( Pair * );
  void reduce_arrays( Bond * );
  void reduce_arrays( Angle * );
  void reduce_arrays( Dihedral * );
  void reduce_arrays( Improper * );
  void reduce_arrays( KSpace * );
  void reduce_arrays( Fix * );

  void reduce_arr_thr( double *, int );
  void reduce_arr_thr( double *, int, int );
  void reduce_arr_thr( int, int, FFT_SCALAR *, FFT_SCALAR * );

  void fdotr_compute( double *, bool );
  static void reset_arr_thr( int, double *, int, int );

 private:
 public:
  double _eng_vdwl;
  double _eng_coul;
  double _virial[6];
  double *_eatom;
  double **_vatom;
  int     _id;
 protected:
  LAMMPS *lmp;
  FixOMPSS *fix;
};

}

typedef struct { double x,y,z;   } dbl3_t;
typedef struct { int a,b,t;      } int3_t;

#endif
