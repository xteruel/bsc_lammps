/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "accelerator_ompss.h"
#include "atom.h"
#include "atom_vec.h"
#include "neigh_list.h"
#include "npair.h"
#include "nbin.h"
#include "comm.h"
#include "domain.h"
#include "neighbor.h"
#include "force.h"
#include "pair.h"
#include "modify.h"
#include "fix_shake.h"
#include "nstencil.h"
#include "memory.h"
#include "comm_brick.h"
#include "comm_brick_ompss.h"


#define MAX_TASK_X_THREAD 18
#define MIN_TASK_X_THREAD 3
#define MAX_NNEIGHT       8

using namespace LAMMPS_NS;

OmpssLMP::OmpssLMP( LAMMPS *lmp ) : Pointers(lmp) {
  ntask       = -1;
  pneight     = NULL;
  nneight     = NULL;
  t_natoms    = NULL;
  t_list      = NULL;
  initialized = 0;
  centinel    = NULL;
  v_cent      = NULL;
  //
  t_nbondlist = NULL;
  t_bondlist  = NULL;
  t_nanglelist = NULL;
  t_anglelist  = NULL;
  t_ndihedrallist = NULL;
  t_dihedrallist  = NULL;
  t_nimproperlist = NULL;
  t_improperlist  = NULL;
  t_nshakelist    = NULL;
  t_shakelist     = NULL;
  f               = NULL;
  torque          = NULL;
  order           = NULL;
  list_sum        = NULL;
  list_inc        = NULL;

  verbose = true;
}

OmpssLMP::~OmpssLMP( ) {
  if ( pneight ) {
    delete [] pneight[0];
    delete [] pneight;
  }

  if ( t_natoms ) delete [] t_natoms;
  if (t_list) {
    if (shared_mem)
      for (int i=0; i< ntask; i++)
        if (t_list[i] ) delete [] t_list[i];
    delete [] t_list;
  }

  if (centinel) delete [] centinel;
  if (v_cent) delete [] v_cent;

  if (t_nbondlist) {
    if (shared_mem) delete [] t_bondlist[0];
    delete [] t_bondlist;
    delete [] t_nbondlist;
  }

  if (t_nanglelist) {
    if (shared_mem)
      if (t_anglelist[0] ) delete [] t_anglelist[0];
    delete [] t_anglelist;
    delete [] t_nanglelist;
  }
  if (t_ndihedrallist) {
    if (shared_mem)
      if (t_dihedrallist[0] ) delete [] t_dihedrallist[0];
    delete [] t_dihedrallist;
    delete [] t_ndihedrallist;
  }

  if (t_nimproperlist) {
    if (shared_mem)
      if (t_improperlist[0] ) delete [] t_improperlist[0];
    delete [] t_improperlist;
    delete [] t_nimproperlist;
  }

  if (t_nshakelist) {
    if (shared_mem)
      if (t_shakelist[0] ) delete [] t_shakelist[0];
    delete [] t_shakelist;
    delete [] t_nshakelist;
  }
  if (f) delete [] f;
  if (torque) delete [] torque;
  if (order) delete [] order;

  if (list_sum) delete [] list_sum;
  if (list_inc) delete [] list_inc;
}

void OmpssLMP::init( ) {
  initialized = 1;
  NBin *nb = force->pair->list->np->nb;
  mbinlo[0] = nb->mbinxlo;
  mbinlo[1] = nb->mbinylo;
  mbinlo[2] = nb->mbinzlo;
  // Get the limits of the bin grid
  double sublo[3], subhi[3];
  if (domain->triclinic)
    domain->bbox(domain->sublo_lamda,domain->subhi_lamda,sublo,subhi);
  else {
    sublo[0] = domain->sublo[0];
    sublo[1] = domain->sublo[1];
    sublo[2] = domain->sublo[2];
    subhi[0] = domain->subhi[0];
    subhi[1] = domain->subhi[1];
    subhi[2] = domain->subhi[2];
  }
  bininv[0] = nb->bininvx;
  bininv[1] = nb->bininvy;
  bininv[2] = nb->bininvz;
  double *bboxlo = neighbor->bboxlo;
  double *bboxhi = neighbor->bboxhi;
  for (int i=0; i< 3; i++) {
    bin_first[i] = (static_cast<int>(floor(sublo[i]-bboxlo[i])*bininv[i]))-mbinlo[i];
    bin_last[i]  = (static_cast<int>(ceil(subhi[i]-bboxlo[i])*bininv[i]))-mbinlo[i];
    grsize[i] = bin_last[i]-bin_first[i];
  }

  gridCut( );

  nneight = new int[ntask];
  pneight = new int*[ntask];
  if (atom->f) f = new double**[ntask];
  if (atom->torque) torque = new double**[ntask];

  if (shared_mem) {
    int *tmp = new int[ntask*MAX_NNEIGHT];
    for (int i=0; i< ntask; i++, tmp += MAX_NNEIGHT) pneight[i] = tmp;
    int t = 0;
    int rsize = taskGrid[0]+1;
    int psize = rsize*(taskGrid[1]+1);

    for (int i=0; i< taskGrid[2]; i++)
    for (int j=0; j< taskGrid[1]; j++)
    for (int k=0; k< taskGrid[0]; k++, t++) {
      pneight[t][0] = k + j*rsize + i*psize;
      pneight[t][1] = pneight[t][0]+1;
      pneight[t][2] = pneight[t][0]+rsize;
      pneight[t][3] = pneight[t][1]+rsize;
      pneight[t][4] = pneight[t][0]+psize;
      pneight[t][5] = pneight[t][1]+psize;
      pneight[t][6] = pneight[t][2]+psize;
      pneight[t][7] = pneight[t][3]+psize;
      nneight[t] = 8;
    }
    for (int i=0; i< 3; i++) {
      div_bins[i] = grsize[i]/taskGrid[i];
      mod_bins[i] = grsize[i]%taskGrid[i];
    }
    computeOrder( );
  } else {
    comm->nthreads = ntask;
    atom->avec->grow( atom->nmax );
    for (int i=0; i< ntask; i++) {
      nneight[i] = 0;
      pneight[i] = nneight;
    }
  }

  if (! t_natoms ) t_natoms = new int[ntask];
  if (! t_list   ) {
    t_list   = new int*[ntask];
    for (int i=0; i< ntask; i++) t_list[i] = NULL;
  }
  int scen = 1;
  for (int i=0; i< 3; i++) if (taskGrid[0]>1) scen *= (taskGrid[0]+1);
  if (! centinel ) {
    centinel = new int[scen];
    memset( centinel, 0, scen*sizeof(int) );
  }
  if (! v_cent) v_cent = new int[ntask];

  if (atom->molecular)
  {
    if (shared_mem) {
      list_sum = new int[comm->nworkers*ntask];
      list_inc = new int[comm->nworkers*ntask];
    }

    if (force->bond) {
      if (! t_nbondlist) t_nbondlist = new int[ntask];
      if (! t_bondlist) {
        t_bondlist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_bondlist[i] = NULL;
      }
    }
    if (force->angle) {
      if (! t_nanglelist) t_nanglelist = new int[ntask];
      if (! t_anglelist) {
        t_anglelist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_anglelist[i] = NULL;
      }
    }
    if (force->dihedral) {
      if (! t_ndihedrallist) t_ndihedrallist = new int[ntask];
      if (! t_dihedrallist) {
        t_dihedrallist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_dihedrallist[i] = NULL;
      }
    }
    if (force->improper) {
      if (! t_nimproperlist) t_nimproperlist = new int[ntask];
      if (! t_improperlist) {
        t_improperlist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_improperlist[i] = NULL;
      }
    }
  }
  if (comm->nworkers>1) {
    if (comm->style==0 && true) {
      CommBrick *oldcomm = static_cast<CommBrick *>(comm);
      comm = new CommBrickOMPSS(oldcomm);
      delete oldcomm;
    }
  }
}

void OmpssLMP::build( ) {
  bool static frst = true;
  if (!initialized) init( );

  for (int i=0; i< ntask; i++) {
    if (f) f[i] = shared_mem? atom->f : atom->f + i*atom->nmax;
    if (torque) torque[i] = shared_mem? atom->torque : atom->torque + i*atom->nmax;
  }

  buildPairList( );

  if (atom->molecular)
  {
    if (force->bond) buildBondList( );
    if (force->angle) buildAngleList( );
    if (force->dihedral) buildDihedralList( );
    if (force->improper) buildImproperList( );
  }
  if (frst) {
    if (!comm->me) printf( "* DONE accelerator %s\n", shared_mem ? "TASK_MODE" : "OpenMP Mode" );
    frst = false;
  }
}

void OmpssLMP::buildPairList( ) {
  NBin *nb = force->pair->list->np->nb;

  if (shared_mem) {
#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(ntask,taskGrid,bin_first,div_bins,mod_bins,nb,atom,t_natoms,t_list)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(ntask,taskGrid,bin_first,div_bins,mod_bins,nb,atom,t_natoms,t_list)
#   endif
    for ( int t=0; t< ntask; t++) {
      int *binhead = nb->binhead;
      int coord[3], ini[3], end[3];
      from1Dto3D( t, taskGrid, coord );
      for ( int i=0; i< 3; i++ ) {
        ini[i] = bin_first[i] + coord[i]*div_bins[i] + MIN(coord[i],mod_bins[i]);
        end[i] = bin_first[i] + (coord[i]+1)*div_bins[i] + MIN(coord[i]+1,mod_bins[i]);
        if (coord[i]==taskGrid[i]-1) end[i]++;
      }
      int n = 0;
      for (int i=ini[2]; i< end[2]; i++)
        for (int j=ini[1]; j< end[1]; j++)
          for (int k=ini[0]; k< end[0]; k++) {
            int b = k + j*nb->mbinx + i*nb->mbinx*nb->mbiny;
            int id = binhead[b];
            while ( id >= 0 ) {
              if ( id < atom->nlocal ) n++;
              id  = nb->bins[id];
            }
          }
      t_natoms[t] = n;
      if (t_list[t]) delete [ ] t_list[t];
      t_list[t] = new int[n];
      n = 0;
      for (int i=ini[2]; i< end[2]; i++)
        for (int j=ini[1]; j< end[1]; j++)
          for (int k=ini[0]; k< end[0]; k++) {
            int b = k + j*nb->mbinx + i*nb->mbinx*nb->mbiny;
            int id = binhead[b];
            while ( id >= 0 ) {
              if ( id < atom->nlocal ) {
                t_list[t][n] = id;
                n++;
              }
              id  = nb->bins[id];
            }
          }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif
  } else {
    //int inum = list->inum;
    int *ilist = force->pair->list->ilist;
    int n = atom->nlocal;
    int di = n/ntask;
    int mo = n%ntask;
    int ini = 0;
    for ( int t=0; t< ntask; t++) {
      int d = di + ( t < mo ? 1 : 0);
      t_natoms[t] = d;
      t_list[t] = ilist + ini;
      ini += d;
    }
  }
}

void OmpssLMP::buildBondList( ) {
  int nbondlist = neighbor->nbondlist;
  int **bondlist = neighbor->bondlist;
  int nlocal = atom->nlocal;
  if (shared_mem) {
    int nworkers = comm->nworkers;
#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,nbondlist,list_sum,ntask,nlocal,bondlist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,nbondlist,list_sum,ntask,nlocal,bondlist)
#   endif
    for (int w=0; w< nworkers; w++) {
      int mo = nbondlist/nworkers;
      int di = nbondlist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_lsum = list_sum + w*ntask;
      memset( t_lsum, 0, ntask*sizeof(int) );
      for (int i=ini; i< end; i++) {
        int a = bondlist[i][0] < nlocal ? bondlist[i][0] : bondlist[i][1];
        int t = atom2Task( a );
        t_lsum[t]++;
      }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif

#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_nbondlist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_nbondlist)
#   endif
    for (int t=0; t< ntask; t++) {
      int n = 0;
      int i = t;
      for (int w=0; w< nworkers; w++, i+=ntask)
        n += list_sum[i];
      t_nbondlist[t] = n;
      i = t;
      list_inc[i] = 0;
      for (int w=1; w< nworkers; w++, i+=ntask)
        list_inc[i+ntask] = list_inc[i] + list_sum[i];
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif
    if (t_bondlist[0]) delete [] t_bondlist[0];
    t_bondlist[0] = nbondlist ? new int*[nbondlist] : NULL;
    for (int t=1; t< ntask; t++)
      t_bondlist[t] = t_bondlist[t-1] + t_nbondlist[t-1];

#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,nbondlist,list_inc,ntask,nlocal,bondlist,t_bondlist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,nbondlist,list_inc,ntask,nlocal,bondlist,t_bondlist)
#   endif
    for (int w=0; w< nworkers; w++) {
      int mo = nbondlist/nworkers;
      int di = nbondlist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_linc = list_inc + w*ntask;
      for (int i=ini; i< end; i++) {
        int a = bondlist[i][0] < nlocal ? bondlist[i][0] : bondlist[i][1];
        int t = atom2Task( a );
        t_bondlist[t][t_linc[t]++] = bondlist[i];
      }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif
  } else {
    int di = nbondlist/ntask;
    int mo = nbondlist%ntask;
    int ini = 0;
    for ( int t=0; t< ntask; t++) {
      int d = di + ( t < mo ? 1 : 0);
      t_nbondlist[t] = d;
      t_bondlist[t] = bondlist + ini;
      ini += d;
    }
  }
}

void OmpssLMP::buildAngleList( ) {
  int nanglelist = neighbor->nanglelist;
  int **anglelist = neighbor->anglelist;
  int nlocal = atom->nlocal;
  if (shared_mem) {
    int nworkers = comm->nworkers;
#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,nanglelist,list_sum,ntask,nlocal,anglelist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,nanglelist,list_sum,ntask,nlocal,anglelist)
#   endif
    for (int w=0; w< nworkers; w++) {
      int mo = nanglelist/nworkers;
      int di = nanglelist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_lsum = list_sum + w*ntask;
      memset( t_lsum, 0, ntask*sizeof(int) );
      for (int i=ini; i< end; i++) {
        int a = anglelist[i][0] < nlocal ? anglelist[i][0] :
              ( anglelist[i][1] < nlocal ? anglelist[i][1] : anglelist[i][2] );
        int t = atom2Task( a );
        t_lsum[t]++;
      }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif

#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_nanglelist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_nanglelist)
#   endif
    for (int t=0; t< ntask; t++) {
      int n = 0;
      int i = t;
      for (int w=0; w< nworkers; w++, i+=ntask)
        n += list_sum[i];
      t_nanglelist[t] = n;
      i = t;
      list_inc[i] = 0;
      for (int w=1; w< nworkers; w++, i+=ntask)
        list_inc[i+ntask] = list_inc[i] + list_sum[i];
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif

    if (t_anglelist[0]) delete [] t_anglelist[0];
    t_anglelist[0] = nanglelist ? new int*[nanglelist] : NULL;
    for (int t=1; t< ntask; t++)
      t_anglelist[t] = t_anglelist[t-1] + t_nanglelist[t-1];

#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,nanglelist,list_inc,ntask,nlocal,anglelist,t_anglelist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,nanglelist,list_inc,ntask,nlocal,anglelist,t_anglelist)
# 	endif
    for (int w=0; w< nworkers; w++) {
      int mo = nanglelist/nworkers;
      int di = nanglelist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_linc = list_inc + w*ntask;
      for (int i=ini; i< end; i++) {
        int a = anglelist[i][0] < nlocal ? anglelist[i][0] :
              ( anglelist[i][1] < nlocal ? anglelist[i][1] : anglelist[i][2] );
        int t = atom2Task( a );
        t_anglelist[t][t_linc[t]++] = anglelist[i];
      }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif
  } else {
    int di = nanglelist/ntask;
    int mo = nanglelist%ntask;
    int ini = 0;
    for ( int t=0; t< ntask; t++) {
      int d = di + ( t < mo ? 1 : 0);
      t_nanglelist[t] = d;
      t_anglelist[t] = anglelist + ini;
      ini += d;
    }
  }
}

void OmpssLMP::buildDihedralList( ) {
  int ndihedrallist = neighbor->ndihedrallist;
  int **dihedrallist = neighbor->dihedrallist;
  int nlocal = atom->nlocal;
  if (shared_mem) {
    int nworkers = comm->nworkers;

# 	if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,ndihedrallist,list_sum,ntask,nlocal,dihedrallist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,ndihedrallist,list_sum,ntask,nlocal,dihedrallist)
# 	endif
    for (int w=0; w< nworkers; w++) {
      int mo = ndihedrallist/nworkers;
      int di = ndihedrallist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_lsum = list_sum + w*ntask;
      memset( t_lsum, 0, ntask*sizeof(int) );
      for (int i=ini; i< end; i++) {
        int a = dihedrallist[i][0] < nlocal ? dihedrallist[i][0] :
              ( dihedrallist[i][1] < nlocal ? dihedrallist[i][1] :
              ( dihedrallist[i][2] < nlocal ? dihedrallist[i][2] : dihedrallist[i][3] ) );
        int t = atom2Task( a );
        t_lsum[t]++;
      }
    }
#   if _OMPSS == 2
# 	pragma oss taskwait
# 	endif

#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_ndihedrallist)
# 	elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_ndihedrallist)
# 	endif
    for (int t=0; t< ntask; t++) {
      int n = 0;
      int i = t;
      for (int w=0; w< nworkers; w++, i+=ntask)
        n += list_sum[i];
      t_ndihedrallist[t] = n;
      i = t;
      list_inc[i] = 0;
      for (int w=1; w< nworkers; w++, i+=ntask)
        list_inc[i+ntask] = list_inc[i] + list_sum[i];
    }
#   if _OMPSS == 2
# 	pragma oss taskwait
# 	endif

    if (t_dihedrallist[0]) delete [] t_dihedrallist[0];
    t_dihedrallist[0] = ndihedrallist ? new int*[ndihedrallist] : NULL;
    for (int t=1; t< ntask; t++)
      t_dihedrallist[t] = t_dihedrallist[t-1] + t_ndihedrallist[t-1];

#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,ndihedrallist,list_inc,ntask,nlocal,dihedrallist,t_dihedrallist)
#   elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,ndihedrallist,list_inc,ntask,nlocal,dihedrallist,t_dihedrallist)
#   endif
    for (int w=0; w< nworkers; w++) {
      int mo = ndihedrallist/nworkers;
      int di = ndihedrallist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_linc = list_inc + w*ntask;
      for (int i=ini; i< end; i++) {
        int a = dihedrallist[i][0] < nlocal ? dihedrallist[i][0] :
              ( dihedrallist[i][1] < nlocal ? dihedrallist[i][1] :
              ( dihedrallist[i][2] < nlocal ? dihedrallist[i][2] : dihedrallist[i][3] ) );
        int t = atom2Task( a );
        t_dihedrallist[t][t_linc[t]++] = dihedrallist[i];
      }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif
  } else {
    int di = ndihedrallist/ntask;
    int mo = ndihedrallist%ntask;
    int ini = 0;
    for ( int t=0; t< ntask; t++) {
      int d = di + ( t < mo ? 1 : 0);
      t_ndihedrallist[t] = d;
      t_dihedrallist[t] = dihedrallist + ini;
      ini += d;
    }
  }
}

void OmpssLMP::buildImproperList( ) {
  int nimproperlist = neighbor->nimproperlist;
  int **improperlist = neighbor->improperlist;
  int nlocal = atom->nlocal;
  if (shared_mem) {
    int nworkers = comm->nworkers;
#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,nimproperlist,list_sum,ntask,nlocal,improperlist)
#   elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,nimproperlist,list_sum,ntask,nlocal,improperlist)
#   endif
    for (int w=0; w< nworkers; w++) {
      int mo = nimproperlist/nworkers;
      int di = nimproperlist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_lsum = list_sum + w*ntask;
      memset( t_lsum, 0, ntask*sizeof(int) );
      for (int i=ini; i< end; i++) {
        int a = improperlist[i][0] < nlocal ? improperlist[i][0] :
              ( improperlist[i][1] < nlocal ? improperlist[i][1] :
              ( improperlist[i][2] < nlocal ? improperlist[i][2] : improperlist[i][3] ) );
        int t = atom2Task( a );
        t_lsum[t]++;
      }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif

#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_nimproperlist)
#   elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(ntask,nworkers,list_sum,list_inc,t_nimproperlist)
#   endif
    for (int t=0; t< ntask; t++) {
      int n = 0;
      int i = t;
      for (int w=0; w< nworkers; w++, i+=ntask)
        n += list_sum[i];
      t_nimproperlist[t] = n;
      i = t;
      list_inc[i] = 0;
      for (int w=1; w< nworkers; w++, i+=ntask)
        list_inc[i+ntask] = list_inc[i] + list_sum[i];
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif

    if (t_improperlist[0]) delete [] t_improperlist[0];
    t_improperlist[0] = nimproperlist ? new int*[nimproperlist] : NULL;
    for (int t=1; t< ntask; t++)
      t_improperlist[t] = t_improperlist[t-1] + t_nimproperlist[t-1];
#   if _OMPSS == 1
#   pragma omp parallel for default(none) \
    shared(nworkers,nimproperlist,list_inc,ntask,nlocal,improperlist,t_improperlist)
#   elif _OMPSS == 2
#   pragma oss task for default(none) \
    shared(nworkers,nimproperlist,list_inc,ntask,nlocal,improperlist,t_improperlist)
#   endif
    for (int w=0; w< nworkers; w++) {
      int mo = nimproperlist/nworkers;
      int di = nimproperlist%nworkers;
      int ini = w*mo + ( w < di ? w : di );
      int end = ini + mo + ( w < di ? 1 : 0 );
      int *t_linc = list_inc + w*ntask;
      for (int i=ini; i< end; i++) {
        int a = improperlist[i][0] < nlocal ? improperlist[i][0] :
              ( improperlist[i][1] < nlocal ? improperlist[i][1] :
              ( improperlist[i][2] < nlocal ? improperlist[i][2] : improperlist[i][3] ) );

        int t = atom2Task( a );
        t_improperlist[t][t_linc[t]++] = improperlist[i];
      }
    }
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif
  } else {
    int di = nimproperlist/ntask;
    int mo = nimproperlist%ntask;
    int ini = 0;
    for ( int t=0; t< ntask; t++) {
      int d = di + ( t < mo ? 1 : 0);
      t_nimproperlist[t] = d;
      t_improperlist[t] = improperlist + ini;
      ini += d;
    }
  }
}

void OmpssLMP::buildShakeList( int nlist, int *list, int **shake_atom ) {
  if (! t_nshakelist) {
    t_nshakelist = new int[ntask];
    if (! t_shakelist) {
      t_shakelist = new int*[ntask];
      for (int i=0; i< ntask; i++) t_shakelist[i] = NULL;
    }
  }
  if (shared_mem) {
    int a, j;
    int nlocal = atom->nlocal;
    memset( t_nshakelist, 0, ntask*sizeof(int) );
    for (int i = 0; i < nlist; i++) {
      int m = list[i];
      j = 0;
      while ( (a=atom->map(shake_atom[m][j]) )>=nlocal) j++;
      int t = atom2Task( a );
      t_nshakelist[t]++;
    }
    for ( int t=0; t< ntask; t++) {
      if (t_shakelist[t]) delete [] t_shakelist[t];
      t_shakelist[t] = t_nshakelist[t] ? new int[t_nshakelist[t]] : NULL;
    }
    memset( t_nshakelist, 0, ntask*sizeof(int) );
    for (int i=0; i< nlist; i++) {
      int m = list[i];
      j = 0;
      while ( (a=atom->map(shake_atom[m][j]) )>=nlocal) j++;
      int t = atom2Task( a );
      t_shakelist[t][t_nshakelist[t]++] = m;
    }
  } else {
    int di = nlist/ntask;
    int mo = nlist%ntask;
    int ini = 0;
    for ( int t=0; t< ntask; t++) {
      int d = di + ( t < mo ? 1 : 0);
      t_nshakelist[t] = d;
      t_shakelist[t] = list + ini;
      ini += d;
    }
  }
}

void OmpssLMP::gridCut( )
{
  if (!comm->me && verbose) printf( "[%d] *** nworkers = %d\n", comm->me, comm->nworkers );
# if defined(_OPENMP) && !defined(_OMPSS)
  shared_mem = false;
  taskGrid[0] = taskGrid[1] = taskGrid[2] = 0;
# else
  NeighList *list = force->pair->list;
  NStencil *st = list->np->ns;
  int grid_stencil = st->sx;
  int ncuts[3];
  if (!comm->me && verbose) printf( "[%d] *** Local Grid = (%d,%d,%d)\n", comm->me, grsize[0], grsize[1], grsize[2] );

  //  Compute a task Size
  int m_x_t = MAX_TASK_X_THREAD + (comm->nworkers < 6 ? 6 - comm->nworkers : 0 );
  int max_ntask = m_x_t*comm->nworkers;
  int tsize     = grid_stencil*3;
  ntask = ((grsize[0]+tsize-1)/tsize)*
          ((grsize[1]+tsize-1)/tsize)*
          ((grsize[2]+tsize-1)/tsize);
  int psize = tsize;
  int ptask = ntask;
  while (ntask>max_ntask) {
    psize = tsize;
    ptask = ntask;
    tsize++;
    ntask = ((grsize[0]+tsize-1)/tsize)*
            ((grsize[1]+tsize-1)/tsize)*
            ((grsize[2]+tsize-1)/tsize);
    if (!comm->me && verbose) printf( "[%d] *** tsize = %d  ntask = %d  max = %d\n", comm->me, tsize, ntask, max_ntask );
  }
  if ( ((float)ntask)/comm->nworkers<12.0 && ((float)ptask)/comm->nworkers<24.0) {
    tsize = psize;
    ntask = ptask;
    if (!comm->me && verbose) printf( "[%d] *** tsize = %d  ntask = %d  max = %d\n", comm->me, tsize, ntask, max_ntask );
  }

  taskGrid[0] = (grsize[0]+tsize-1)/tsize;
  taskGrid[1] = (grsize[1]+tsize-1)/tsize;
  taskGrid[2] = (grsize[2]+tsize-1)/tsize;
  if (!comm->me && verbose) printf( "[%d] *** tsize = %d  tskgr = %d %d %d\n", comm->me, tsize, taskGrid[0], taskGrid[1], taskGrid[2] );

  int wload_t = grsize[0]* grsize[1]*grsize[2]/(tsize*tsize*tsize)/comm->nworkers;
  if (!comm->me && verbose) printf( "[%d] *** wload_t %d  ntask_t= %d\n", comm->me, wload_t, ntask/comm->nworkers );

  // Task/Wload per thread
  shared_mem = wload_t >= MAX_NNEIGHT;
  if (shared_mem) {
    int *numneigh = list->numneigh;
    int wload = 0;
#   if _OMPSS == 1
#   pragma omp parallel for reduction(+:wload)
#   elif _OMPSS == 2
#   pragma oss task for reduction(+:wload)
#   endif
    for (int i=0; i< atom->nlocal; i++) wload += numneigh[i];
#   if _OMPSS == 2
#   pragma oss taskwait
#   endif
    wload = wload/comm->nworkers;
    if (!strcmp("gran/hooke/history/ompss",force->pair_style)) wload = wload * 5;
    if (!comm->me && verbose) printf( "[%d] *** wload = %d  min=%d\n", comm->me, wload, MIN_WLOAD_X_THREAD );
    if (wload<MIN_WLOAD_X_THREAD) shared_mem = false;
  }
# endif
  if (!shared_mem) {
    ntask = comm->nworkers;
    if (!comm->me && verbose) { printf( "[%d] *** PRIVATIZE ON(%d)\n", comm->me, ntask ); fflush(stdout); }
  }
}

void OmpssLMP::computeOrder( )
{
  order = new int[ntask];
  int id[3] = { 0, 1, 2 };
  for (int i=0; i< 2; i++)
    for (int j=i+1; j< 3; j++)
      if (taskGrid[id[j]]>taskGrid[id[i]]) {
        int k = id[i];
        id[i] = id[j];
        id[j] = k;
      }
  int ld[3] = { 1, taskGrid[0], taskGrid[0]*taskGrid[1] };
  int imax=taskGrid[id[2]];
  int jmax=taskGrid[id[1]];
  int kmax=taskGrid[id[0]];
  int ild = ld[id[2]];
  int jld = ld[id[1]];
  int kld = ld[id[0]];
  int ind = 0;

  for (int i1=0; i1< 2; i1++)
  for (int j1=0; j1< 2; j1++)
  for (int k1=0; k1< 2; k1++)
  for (int i2=i1; i2< imax; i2 += 2)
  for (int j2=j1; j2< jmax; j2 += 2)
  for (int k2=k1; k2< kmax; k2 += 2)
    order[ind++] = k2*kld + j2*jld + i2*ild;
}

inline void OmpssLMP::from1Dto3D( int i, int *X, int *vec ) {
  vec[0] = i%X[0];
  vec[1] = (i/X[0])%X[1];
  vec[2] = i/(X[0]*X[1]);
}

inline int OmpssLMP::atom2Task( int a ) {
  double *bboxlo = neighbor->bboxlo;
  double *bboxhi = neighbor->bboxhi;
  int task[3];
  for (int i=0; i< 3; i++) {
    int bin = (static_cast<int>((atom->x[a][i]-bboxlo[i])*bininv[i])) - mbinlo[i]- bin_first[i];
    if  ( bin>=(grsize[i]-1) ) task[i] = taskGrid[i]-1;
    else if (mod_bins[i]==0) task[i] = bin/div_bins[i];
    else {
      task[i]= bin/(div_bins[i]+1);
      if (task[i]>=mod_bins[i]) task[i] = (bin-mod_bins[i])/div_bins[i];
    }
  }
  return task[0] + task[1]*taskGrid[0] + task[2]*taskGrid[0]*taskGrid[1];
}
