/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef LMP_ACCELERATOR_OMPSS_H
#define LMP_ACCELERATOR_OMPSS_H

#include "pointers.h"

#define MIN_WLOAD_X_THREAD  131072

namespace LAMMPS_NS {

class OmpssLMP : protected Pointers {
 public:
  OmpssLMP( LAMMPS *lmp );
  ~OmpssLMP( );

  void init( );
  void build( );
  int GetNTask( ) { return ntask; }

  int **pneight;   // List of neighbours of every task
  int *nneight;

  int **t_list;
  int *t_natoms;
  int *centinel;
  int *v_cent;

  // Bond list
  int *t_nbondlist;
  int ***t_bondlist;

  // Angle list
  int *t_nanglelist;
  int ***t_anglelist;

  // Dihedral list
  int *t_ndihedrallist;
  int ***t_dihedrallist;

  // Improper list
  int *t_nimproperlist;
  int ***t_improperlist;

  // Shake list
  int *t_nshakelist;
  int **t_shakelist;

  int  initialized;

  bool shared_mem;
  double ***f;
  double ***torque;

  int GetTask( int t ) { return order ? order[t] : t; }

  void buildShakeList( int, int *, int ** );

 private:
  int *order;
  int *list_sum;
  int *list_inc;

  void gridCut( );
  void computeOrder( );

  void buildPairList( );
  void buildBondList( );
  void buildAngleList( );
  void buildDihedralList( );
  void buildImproperList( );


  void from1Dto3D( int, int *, int * );
  int atom2Task( int );

  int ntask;        // Number of tasks

  int taskGrid[3];  // Tasks grid

  int bin_first[3]; // Index of first bin of self atoms
  int bin_last[3];  // Index of last bin of self atoms
  int grsize[3];    // Number of bins of self atoms

  int mbinlo[3];    // Index of first bin in current domain

  int div_bins[3];  // Division between bins & tasks
  int mod_bins[3];  // Module of the division between bins & tasks
  double bininv[3]; // Inverse of bin sizes

  bool verbose;
};

}

#endif
