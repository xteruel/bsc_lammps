/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <stdio.h>
#include <string.h>
#include "fix_nve_ompss.h"
#include "atom.h"
#include "force.h"
#include "comm.h"
#include "accelerator_ompss.h"

using namespace LAMMPS_NS;
using namespace FixConst;

/* ---------------------------------------------------------------------- */

FixNVEOMPSS::FixNVEOMPSS(LAMMPS *lmp, int narg, char **arg) :
  FixNVE(lmp, narg, arg) { }

/* ---------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   allow for both per-type and per-atom mass
------------------------------------------------------------------------- */

void FixNVEOMPSS::initial_integrate(int vflag)
{
  // update v and x of atoms in group

  double **x = atom->x;
  double **v = atom->v;
  double **f = atom->f;
  double *rmass = atom->rmass;
  double *mass = atom->mass;
  int *type = atom->type;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  if (igroup == atom->firstgroup) nlocal = atom->nfirst;

  int ntask = comm->nworkers;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(nlocal,ntask,rmass,mask,mass,type,f,v,x)
# endif
  for (int t=0; t< ntask; t++) {
#   if _OMPSS == 2
#   pragma oss task default(none) firstprivate(t) \
  	shared(nlocal,ntask,rmass,mask,mass,type,f,v,x)
#   endif
    {

    int di = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );
    if (rmass) {
      for (int i = ini; i < end; i++)
        if (mask[i] & groupbit) {
          double dtfm = dtf / rmass[i];
          v[i][0] += dtfm * f[i][0];
          v[i][1] += dtfm * f[i][1];
          v[i][2] += dtfm * f[i][2];
          x[i][0] += dtv * v[i][0];
          x[i][1] += dtv * v[i][1];
          x[i][2] += dtv * v[i][2];
        }
    } else {
      for (int i = ini; i < end; i++)
        if (mask[i] & groupbit) {
          double dtfm = dtf / mass[type[i]];
          v[i][0] += dtfm * f[i][0];
          v[i][1] += dtfm * f[i][1];
          v[i][2] += dtfm * f[i][2];
          x[i][0] += dtv * v[i][0];
          x[i][1] += dtv * v[i][1];
          x[i][2] += dtv * v[i][2];
        }
    }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}

/* ---------------------------------------------------------------------- */

void FixNVEOMPSS::final_integrate()
{
  // update v of atoms in group

  double **v = atom->v;
  double **f = atom->f;
  double *rmass = atom->rmass;
  double *mass = atom->mass;
  int *type = atom->type;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;
  if (igroup == atom->firstgroup) nlocal = atom->nfirst;


  int ntask = comm->nworkers;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nlocal,rmass,mask,mass,type,f,v)
# endif
  for (int t=0; t< ntask; t++) {
#   if _OMPSS == 2
#   pragma oss task default(none) firstprivate(t) \
  	shared(ntask,nlocal,rmass,mask,mass,type,f,v)
# 	endif
    {
    int di = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );
    if (rmass) {
      for (int i = ini; i < end; i++)
        if (mask[i] & groupbit) {
          double dtfm = dtf / rmass[i];
          v[i][0] += dtfm * f[i][0];
          v[i][1] += dtfm * f[i][1];
          v[i][2] += dtfm * f[i][2];
        }

    } else {
      for (int i = ini; i < end; i++)
        if (mask[i] & groupbit) {
          double dtfm = dtf / mass[type[i]];
          v[i][0] += dtfm * f[i][0];
          v[i][1] += dtfm * f[i][1];
          v[i][2] += dtfm * f[i][2];
        }
    }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
}
