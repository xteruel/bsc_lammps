/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "npair_half_bin_newton_ompss.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "atom.h"
#include "atom_vec.h"
#include "group.h"
#include "molecule.h"
#include "domain.h"
#include "my_page.h"
#include "error.h"
#include "comm.h"
#include "omp.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

NPairHalfBinNewtonOmpss::NPairHalfBinNewtonOmpss(LAMMPS *lmp) : NPair(lmp) {}

/* ----------------------------------------------------------------------
   binned neighbor list construction with full Newton's 3rd law
   each owned atom i checks its own bin and other bins in Newton stencil
   every pair stored exactly once by some processor
------------------------------------------------------------------------- */

void NPairHalfBinNewtonOmpss::build(NeighList *list)
{
  int ntask = comm->nworkers;
  int nlocal = (includegroup) ? atom->nfirst : atom->nlocal;
  int molecular = atom->molecular;
  int moltemplate = (molecular == 2) ? 1 : 0;

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nlocal,list,molecular,moltemplate)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,nlocal,list,molecular,moltemplate)
# endif
  for (int t=0; t < ntask; t++)
  {
    int di = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );

    int          *type = atom->type;
    int          *mask = atom->mask;
    double         **x = atom->x;
    int      *molindex = atom->molindex;
    int       *molatom = atom->molatom;
    tagint        *tag = atom->tag;
    tagint   *molecule = atom->molecule;
    tagint   **special = atom->special;
    int     **nspecial = atom->nspecial;
    Molecule **onemols = atom->avec->onemols;
    int      *numneigh = list->numneigh;
    int   **firstneigh = list->firstneigh;
    int         *ilist = list->ilist;

    MyPage<int> *ipage = new MyPage<int>( &(list->ipage[t]) );
    ipage->reset();
    
    for (int i = ini; i < end; i++) {
      int *neighptr = ipage->vget();
      int    itype = type[i];
      double  xtmp = x[i][0];
      double  ytmp = x[i][1];
      double  ztmp = x[i][2];
      int        n = 0;

      int imol, iatom, tagprev;
      if (moltemplate) {
        imol = molindex[i];
        iatom = molatom[i];
        tagprev = tag[i] - iatom - 1;
      }

      // loop over rest of atoms in i's bin, ghosts are at end of linked list
      // if j is owned atom, store it, since j is beyond i in linked list
      // if j is ghost, only store if j coords are "above and to the right" of i
      for (int j = bins[i]; j >= 0; j = bins[j]) {
        if (j >= nlocal) {
          if (x[j][2] < ztmp) continue;
          if (x[j][2] == ztmp) {
            if (x[j][1] < ytmp) continue;
            if (x[j][1] == ytmp && x[j][0] < xtmp) continue;
          }
        }
        int jtype = type[j];
        if (exclude && exclusion( i, j, itype, jtype, mask, molecule ) ) continue;

        double delx = xtmp - x[j][0];
        double dely = ytmp - x[j][1];
        double delz = ztmp - x[j][2];
        double rsq = delx*delx + dely*dely + delz*delz;
        if (rsq <= cutneighsq[itype][jtype]) {
          if (molecular) {
            int which = 0;
            if (!moltemplate)
              which = find_special( special[i], nspecial[i], tag[j] );
            else if (imol >= 0)
              which = find_special( onemols[imol]->special[iatom],
                                    onemols[imol]->nspecial[iatom],
                                    tag[j]-tagprev );
            if (which == 0) neighptr[n++] = j;
            else if (domain->minimum_image_check(delx,dely,delz)) neighptr[n++] = j;
            else if (which > 0) neighptr[n++] = j ^ (which << SBBITS);
            // OLD: if (which >= 0) neighptr[n++] = j ^ (which << SBBITS);
          } else neighptr[n++] = j;
        }
      }
      // loop over all atoms in other bins in stencil, store every pair
      int ibin = atom2bin[i];
      for (int k = 0; k < nstencil; k++) {
        for (int j = binhead[ibin+stencil[k]]; j >= 0; j = bins[j]) {
          int jtype = type[j];
          if ( exclude && exclusion( i, j, itype, jtype, mask, molecule ) ) continue;

          double delx = xtmp - x[j][0];
          double dely = ytmp - x[j][1];
          double delz = ztmp - x[j][2];
          double rsq = delx*delx + dely*dely + delz*delz;
          if (rsq <= cutneighsq[itype][jtype]) {
            if (molecular) {
              int which = 0;
              if (!moltemplate)
                which = find_special( special[i], nspecial[i], tag[j] );
              else if (imol >= 0)
                which = find_special( onemols[imol]->special[iatom],
                                      onemols[imol]->nspecial[iatom],
                                      tag[j]-tagprev );
              if (which == 0) neighptr[n++] = j;
              else if (domain->minimum_image_check(delx,dely,delz)) neighptr[n++] = j;
              else if (which > 0) neighptr[n++] = j ^ (which << SBBITS);
              // OLD: if (which >= 0) neighptr[n++] = j ^ (which << SBBITS);
            } else neighptr[n++] = j;
          }
        }
      }
      numneigh[i] = n;
      ilist[i] = i;
      firstneigh[i] = neighptr;
      ipage->vgot(n);
      if (ipage->status())
        error->one(FLERR,"Neighbor list overflow, boost neigh_modify one");
    }
    delete ipage;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  list->inum = nlocal;
}
