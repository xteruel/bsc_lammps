/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <mpi.h>
#include <string.h>
#include "compute_temp.h"
#include "atom.h"
#include "update.h"
#include "force.h"
#include "domain.h"
#include "comm.h"
#include "group.h"
#include "error.h"
#include "accelerator_ompss.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

ComputeTemp::ComputeTemp(LAMMPS *lmp, int narg, char **arg) :
  Compute(lmp, narg, arg)
{
  if (narg != 3) error->all(FLERR,"Illegal compute temp command");

  scalar_flag = vector_flag = 1;
  size_vector = 6;
  extscalar = 0;
  extvector = 1;
  tempflag = 1;

  vector = new double[6];
}

/* ---------------------------------------------------------------------- */

ComputeTemp::~ComputeTemp()
{
  if (!copymode)
    delete [] vector;
}

/* ---------------------------------------------------------------------- */

void ComputeTemp::setup()
{
  dynamic = 0;
  if (dynamic_user || group->dynamic[igroup]) dynamic = 1;
  dof_compute();
}

/* ---------------------------------------------------------------------- */

void ComputeTemp::dof_compute()
{
  adjust_dof_fix();
  natoms_temp = group->count(igroup);
  dof = domain->dimension * natoms_temp;
  dof -= extra_dof + fix_dof;
  if (dof > 0.0) tfactor = force->mvv2e / (dof * force->boltz);
  else tfactor = 0.0;
}

/* ---------------------------------------------------------------------- */
double ComputeTemp::compute_scalar()
{
  invoked_scalar = update->ntimestep;

  double **v = atom->v;
  double *mass = atom->mass;
  double *rmass = atom->rmass;
  int *type = atom->type;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;

  double temp = 0.0;

  int ntask = comm->nworkers;

#   if _OMPSS == 1 || defined(_OPENMP)
#   pragma omp parallel for default(none) reduction(+:temp) \
    shared(nlocal,ntask,mask,v,rmass,mass,type)
#   elif _OMPSS == 2
#   pragma oss task for default(none) reduction(+:temp) \
    shared(nlocal,ntask,mask,v,rmass,mass,type)
#   endif
  for (int t=0; t< ntask; t++) {
    int st = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*st + ( t < mo ? t : mo );
    int end = ini + st + ( t < mo ? 1 : 0 );
    double tt=0.0;
    if (rmass) {
      for (int i = ini; i < end; i++)
        if (mask[i] & groupbit)
          tt += (v[i][0]*v[i][0] + v[i][1]*v[i][1] + v[i][2]*v[i][2]) * rmass[i];
    } else {
      for (int i = ini; i < end; i++)
        if (mask[i] & groupbit)
          tt += (v[i][0]*v[i][0] + v[i][1]*v[i][1] + v[i][2]*v[i][2]) *
            mass[type[i]];
    }
    temp += tt;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  MPI_Allreduce(&temp,&scalar,1,MPI_DOUBLE,MPI_SUM,world);
  if (dynamic) dof_compute();
  if (dof < 0.0 && natoms_temp > 0.0)
    error->all(FLERR,"Temperature compute degrees of freedom < 0");
  scalar *= tfactor;
  return scalar;
}
/* ---------------------------------------------------------------------- */

void ComputeTemp::compute_vector()
{
  int i;

  invoked_vector = update->ntimestep;

  double **v = atom->v;
  double *mass = atom->mass;
  double *rmass = atom->rmass;
  int *type = atom->type;
  int *mask = atom->mask;
  int nlocal = atom->nlocal;

  double massone;
  double t[6] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
  for (i = 0; i < 6; i++) t[i] = 0.0;

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:t) \
  shared(nlocal,mask,rmass,mass,type,v)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:t) \
  shared(nlocal,mask,rmass,mass,type,v)
# endif
  for (i = 0; i < nlocal; i++)
    if (mask[i] & groupbit) {
      double massone = rmass ? rmass[i] : mass[type[i]];
      t[0] += massone * v[i][0]*v[i][0];
      t[1] += massone * v[i][1]*v[i][1];
      t[2] += massone * v[i][2]*v[i][2];
      t[3] += massone * v[i][0]*v[i][1];
      t[4] += massone * v[i][0]*v[i][2];
      t[5] += massone * v[i][1]*v[i][2];
    }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  MPI_Allreduce(t,vector,6,MPI_DOUBLE,MPI_SUM,world);
  for (i = 0; i < 6; i++) vector[i] *= force->mvv2e;
}
