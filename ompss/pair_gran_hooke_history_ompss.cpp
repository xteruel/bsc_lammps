/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Leo Silbert (SNL), Gary Grest (SNL)
------------------------------------------------------------------------- */

#include <math.h>
#include <string.h>
#include "pair_gran_hooke_history_ompss.h"
#include "fix_neigh_history.h"
#include "atom.h"
#include "comm.h"
#include "fix.h"
#include "force.h"
#include "memory.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "update.h"

#include "accelerator_ompss.h"

#include "suffix.h"
using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

PairGranHookeHistoryOMPSS::PairGranHookeHistoryOMPSS(LAMMPS *lmp)
  : PairGranHookeHistory(lmp), ThrOMPSS(lmp)
{
  suffix_flag |= Suffix::OMPSS;
}


/* ---------------------------------------------------------------------- */

void PairGranHookeHistoryOMPSS::compute(int eflag, int vflag)
{
  if (eflag || vflag) {
    ev_setup(eflag,vflag);
  } else evflag = vflag_fdotr = 0;

  int shearupdate = (update->setupflag) ? 0 : 1;

  // update rigid body info for owned & ghost atoms if using FixRigid masses
  // body[i] = which body atom I is in, -1 if none
  // mass_body = mass of each rigid body

  if (fix_rigid && neighbor->ago == 0) {
    int tmp;
    int *body = (int *) fix_rigid->extract("body",tmp);
    double *mass_body = (double *) fix_rigid->extract("masstotal",tmp);
    if (atom->nmax > nmax) {
      memory->destroy(mass_rigid);
      nmax = atom->nmax;
      memory->create(mass_rigid,nmax,"pair:mass_rigid");
    }
    int nlocal = atom->nlocal;
    for (int i = 0; i < nlocal; i++)
      if (body[i] >= 0) mass_rigid[i] = mass_body[body[i]];
      else mass_rigid[i] = 0.0;
    comm->forward_comm_pair(this);
  }

  const int nall = atom->nlocal + atom->nghost;
  const int nthreads = comm->nthreads;

  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none) \
  shared(eflag,vflag,shearupdate)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if     _OMPSS == 1
#   pragma omp task default(none) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    firstprivate(t) shared(eflag,vflag,shearupdate)
#   elif   _OMPSS == 2
#   pragma oss task default(none) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    firstprivate(t) shared(eflag,vflag,shearupdate)
#   endif
    {
      ThrOMPSS thr;
      thr.init( (Pair *)this, ompss->shared_mem ? 0 : atom->nmax );
      if (evflag) {
        if (shearupdate) {
          if (force->newton_pair) eval<1,1,1>( t, &thr );
          else                    eval<1,1,0>( t, &thr );
        } else {
          if (force->newton_pair) eval<1,0,1>( t, &thr );
          else                    eval<1,0,0>( t, &thr );
        }
      } else {
        if (shearupdate) {
          if (force->newton_pair) eval<0,1,1>( t, &thr );
          else                    eval<0,1,0>( t, &thr );
        } else {
          if (force->newton_pair) eval<0,0,1>( t, &thr );
          else                    eval<0,0,0>( t, &thr );
        }
      }
      thr.reduce_thr( this, eflag, vflag );
    }
  }
  reduce_arrays( this );
}

template <int EVFLAG, int SHEARUPDATE, int NEWTON_PAIR>
void PairGranHookeHistoryOMPSS::eval( int t, ThrOMPSS * const thr)
{
  double fx,fy,fz;
  double vtr1,vtr2,vtr3,vrel;
  double tor1,tor2,tor3;
  double fn,fs,fs1,fs2,fs3;
  double shrmag,rsht;

  const dbl3_t * _noalias const x = (dbl3_t *) atom->x[0];
  const dbl3_t * _noalias const v = (dbl3_t *) atom->v[0];
  const dbl3_t * _noalias const omega = (dbl3_t *) atom->omega[0];

  const double * const radius = atom->radius;
  const double * const rmass = atom->rmass;
  dbl3_t * _noalias const f = (dbl3_t *) ompss->f[t][0];
  dbl3_t * _noalias const torque = (dbl3_t *) ompss->torque[t][0];

  const int * const mask = atom->mask;
  const int nlocal = atom->nlocal;

  const int    * _noalias const ilist = ompss->t_list[t];
  const int    * _noalias const numneigh = list->numneigh;
  int    * const * const firstneigh = list->firstneigh;
  double * const * const firstshear = fix_history->firstvalue;
  int    * const * const firsttouch = fix_history->firstflag;

  // loop over neighbors of my atoms

  const int natoms = ompss->t_natoms[t];

  for (int ii = 0; ii < natoms; ++ii) {
    const int i = ilist[ii];
    double const xtmp = x[i].x;
    double const ytmp = x[i].y;
    double const ztmp = x[i].z;
    const double radi = radius[i];
    int * const touch = firsttouch[i];
    double * const allshear = firstshear[i];
    int * jlist = firstneigh[i];
    const int jnum = numneigh[i];
    double fxtmp,fytmp,fztmp;
    double t1tmp,t2tmp,t3tmp;
    fxtmp=fytmp=fztmp=t1tmp=t2tmp=t3tmp=0.0;

    for (int jj = 0; jj < jnum; jj++) {
      int j = jlist[jj];
      j &= NEIGHMASK;

      double myshear[3];

      double delx = xtmp - x[j].x;
      double dely = ytmp - x[j].y;
      double delz = ztmp - x[j].z;
      const double rsq = delx*delx + dely*dely + delz*delz;
      const double radj = radius[j];
      const double radsum = radi + radj;

      if (rsq >= radsum*radsum) {

        // unset non-touching neighbors

        touch[jj] = 0;
        myshear[0] = 0.0;
        myshear[1] = 0.0;
        myshear[2] = 0.0;

      } else {
        const double r = sqrt(rsq);
        const double rinv = 1.0/r;
        const double rsqinv = 1.0/rsq;

        // relative translational velocity

        const double vr1 = v[i].x - v[j].x;
        const double vr2 = v[i].y - v[j].y;
        const double vr3 = v[i].z - v[j].z;

        // normal component

        const double vnnr = vr1*delx + vr2*dely + vr3*delz;
        const double vn1 = delx*vnnr * rsqinv;
        const double vn2 = dely*vnnr * rsqinv;
        const double vn3 = delz*vnnr * rsqinv;

        // tangential component

        const double vt1 = vr1 - vn1;
        const double vt2 = vr2 - vn2;
        const double vt3 = vr3 - vn3;

        // relative rotational velocity

        const double wr1 = (radi*omega[i].x + radj*omega[j].x) * rinv;
        const double wr2 = (radi*omega[i].y + radj*omega[j].y) * rinv;
        const double wr3 = (radi*omega[i].z + radj*omega[j].z) * rinv;

        // meff = effective mass of pair of particles
        // if I or J part of rigid body, use body mass
        // if I or J is frozen, meff is other particle

        double mi = rmass[i];
        double mj = rmass[j];
        if (fix_rigid) {
          if (mass_rigid[i] > 0.0) mi = mass_rigid[i];
          if (mass_rigid[j] > 0.0) mj = mass_rigid[j];
        }

        double meff = mi*mj / (mi+mj);
        if (mask[i] & freeze_group_bit) meff = mj;
        if (mask[j] & freeze_group_bit) meff = mi;

        // normal forces = Hookian contact + normal velocity damping

        const double damp = meff*gamman*vnnr*rsqinv;
        const double ccel = kn*(radsum-r)*rinv - damp;

        // relative velocities

        vtr1 = vt1 - (delz*wr2-dely*wr3);
        vtr2 = vt2 - (delx*wr3-delz*wr1);
        vtr3 = vt3 - (dely*wr1-delx*wr2);
        vrel = vtr1*vtr1 + vtr2*vtr2 + vtr3*vtr3;
        vrel = sqrt(vrel);

        // shear history effects

        touch[jj] = 1;
        memcpy(myshear,allshear + 3*jj, 3*sizeof(double));

        if (SHEARUPDATE) {
          myshear[0] += vtr1*dt;
          myshear[1] += vtr2*dt;
          myshear[2] += vtr3*dt;
        }
        shrmag = sqrt(myshear[0]*myshear[0] + myshear[1]*myshear[1] +
                      myshear[2]*myshear[2]);

        // rotate shear displacements

        rsht = myshear[0]*delx + myshear[1]*dely + myshear[2]*delz;
        rsht *= rsqinv;
        if (SHEARUPDATE) {
          myshear[0] -= rsht*delx;
          myshear[1] -= rsht*dely;
          myshear[2] -= rsht*delz;
        }

        // tangential forces = shear + tangential velocity damping

        fs1 = - (kt*myshear[0] + meff*gammat*vtr1);
        fs2 = - (kt*myshear[1] + meff*gammat*vtr2);
        fs3 = - (kt*myshear[2] + meff*gammat*vtr3);

        // rescale frictional displacements and forces if needed

        fs = sqrt(fs1*fs1 + fs2*fs2 + fs3*fs3);
        fn = xmu * fabs(ccel*r);

        if (fs > fn) {
          if (shrmag != 0.0) {
            const double fnfs = fn/fs;
            const double mgkt = meff*gammat/kt;
            myshear[0] = fnfs * (myshear[0] + mgkt*vtr1) - mgkt*vtr1;
            myshear[1] = fnfs * (myshear[1] + mgkt*vtr2) - mgkt*vtr2;
            myshear[2] = fnfs * (myshear[2] + mgkt*vtr3) - mgkt*vtr3;
            fs1 *= fnfs;
            fs2 *= fnfs;
            fs3 *= fnfs;
          } else fs1 = fs2 = fs3 = 0.0;
        }

        // forces & torques

        fx = delx*ccel + fs1;
        fy = dely*ccel + fs2;
        fz = delz*ccel + fs3;
        fxtmp  += fx;
        fytmp  += fy;
        fztmp  += fz;

        const double tor1 = rinv * (dely*fs3 - delz*fs2);
        const double tor2 = rinv * (delz*fs1 - delx*fs3);
        const double tor3 = rinv * (delx*fs2 - dely*fs1);
        t1tmp -= radi*tor1;
        t2tmp -= radi*tor2;
        t3tmp -= radi*tor3;

        if (NEWTON_PAIR || j < nlocal) {
          f[j].x -= fx;
          f[j].y -= fy;
          f[j].z -= fz;
          torque[j].x -= radj*tor1;
          torque[j].y -= radj*tor2;
          torque[j].z -= radj*tor3;
        }

        if (EVFLAG) thr->ev_tally_xyz_thr( this, i, j, nlocal, NEWTON_PAIR,
                                           0.0, 0.0, fx, fy, fz, delx, dely, delz );
      }
      memcpy(allshear + 3*jj, myshear, 3*sizeof(double));
    }
    f[i].x += fxtmp;
    f[i].y += fytmp;
    f[i].z += fztmp;
    torque[i].x += t1tmp;
    torque[i].y += t2tmp;
    torque[i].z += t3tmp;
  }
}
