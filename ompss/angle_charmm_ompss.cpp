/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing author: Paul Crozier (SNL)
------------------------------------------------------------------------- */

#include "angle_charmm_ompss.h"
#include "atom.h"
#include "comm.h"
#include "force.h"
#include "neighbor.h"
#include "domain.h"
#include "math_const.h"
#include "accelerator_ompss.h"

#include <math.h>

#include "suffix.h"
using namespace LAMMPS_NS;
using namespace MathConst;

#define SMALL 0.001

/* ---------------------------------------------------------------------- */

AngleCharmmOMPSS::AngleCharmmOMPSS(LAMMPS *lmp)
  : AngleCharmm(lmp), ThrOMPSS(lmp)
{
  suffix_flag |= Suffix::OMPSS;
}

/* ---------------------------------------------------------------------- */

void AngleCharmmOMPSS::compute(int eflag, int vflag)
{

  if (eflag || vflag) {
    ev_setup(eflag,vflag);
  } else evflag = 0;

  const int nall = atom->nlocal + atom->nghost;
  const int nthreads = comm->nthreads;
  int inum = neighbor->nanglelist;

  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none) \
  shared(eflag,vflag,inum)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
# 	if   _OMPSS == 1
#   pragma omp task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    shared(evflag,eflag,vflag,force,inum)
# 	elif _OMPSS == 2
#   pragma oss task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    shared(evflag,eflag,vflag,force,inum)
#   endif
    {

      if (inum > 0) {
        ThrOMPSS thr;
        thr.init( (Angle *)this, ompss->shared_mem ? 0 : atom->nmax );
        if (evflag) {
          if (eflag) {
            if (force->newton_bond) eval<1,1,1>( t, &thr );
            else                    eval<1,1,0>( t, &thr );
          } else {
            if (force->newton_bond) eval<1,0,1>( t, &thr );
            else                    eval<1,0,0>( t, &thr );
          }
        } else {
          if (force->newton_bond) eval<0,0,1>( t, &thr );
          else                    eval<0,0,0>( t, &thr );
        }
        thr.reduce_thr( this, eflag, vflag );
      }
    } // end of omp parallel region
  }
  reduce_arrays( this );
}


template <int EVFLAG, int EFLAG, int NEWTON_BOND>
void AngleCharmmOMPSS::eval( int t, ThrOMPSS * const thr )
{
  int i1,i2,i3,n,type;
  double delx1,dely1,delz1,delx2,dely2,delz2;
  double eangle,f1[3],f3[3];
  double dtheta,tk;
  double rsq1,rsq2,r1,r2,c,s,a,a11,a12,a22;
  double delxUB,delyUB,delzUB,rsqUB,rUB,dr,rk,forceUB;

  const dbl3_t * _noalias const x = (dbl3_t *) atom->x[0];
  dbl3_t * _noalias const f = (dbl3_t *) ompss->f[t][0];
  const int * const * const anglelist = ompss->t_anglelist[t];
  const int nlocal = atom->nlocal;
  eangle = 0.0;

  const int nangle = ompss->t_nanglelist[t];
  for (n = 0; n < nangle; n++) {
    i1 = anglelist[n][0];
    i2 = anglelist[n][1];
    i3 = anglelist[n][2];
    type = anglelist[n][3];

    // 1st bond

    delx1 = x[i1].x - x[i2].x;
    dely1 = x[i1].y - x[i2].y;
    delz1 = x[i1].z - x[i2].z;

    rsq1 = delx1*delx1 + dely1*dely1 + delz1*delz1;
    r1 = sqrt(rsq1);

    // 2nd bond

    delx2 = x[i3].x - x[i2].x;
    dely2 = x[i3].y - x[i2].y;
    delz2 = x[i3].z - x[i2].z;

    rsq2 = delx2*delx2 + dely2*dely2 + delz2*delz2;
    r2 = sqrt(rsq2);

    // Urey-Bradley bond

    delxUB = x[i3].x - x[i1].x;
    delyUB = x[i3].y - x[i1].y;
    delzUB = x[i3].z - x[i1].z;

    rsqUB = delxUB*delxUB + delyUB*delyUB + delzUB*delzUB;
    rUB = sqrt(rsqUB);

    // Urey-Bradley force & energy

    dr = rUB - r_ub[type];
    rk = k_ub[type] * dr;

    if (rUB > 0.0) forceUB = -2.0*rk/rUB;
    else forceUB = 0.0;

    if (EFLAG) eangle = rk*dr;

    // angle (cos and sin)

    c = delx1*delx2 + dely1*dely2 + delz1*delz2;
    c /= r1*r2;

    if (c > 1.0) c = 1.0;
    if (c < -1.0) c = -1.0;

    s = sqrt(1.0 - c*c);
    if (s < SMALL) s = SMALL;
    s = 1.0/s;

    // harmonic force & energy

    dtheta = acos(c) - theta0[type];
    tk = k[type] * dtheta;

    if (EFLAG) eangle += tk*dtheta;

    a = -2.0 * tk * s;
    a11 = a*c / rsq1;
    a12 = -a / (r1*r2);
    a22 = a*c / rsq2;

    f1[0] = a11*delx1 + a12*delx2 - delxUB*forceUB;
    f1[1] = a11*dely1 + a12*dely2 - delyUB*forceUB;
    f1[2] = a11*delz1 + a12*delz2 - delzUB*forceUB;

    f3[0] = a22*delx2 + a12*delx1 + delxUB*forceUB;
    f3[1] = a22*dely2 + a12*dely1 + delyUB*forceUB;
    f3[2] = a22*delz2 + a12*delz1 + delzUB*forceUB;

    // apply force to each of 3 atoms

    if (NEWTON_BOND || i1 < nlocal) {
      f[i1].x += f1[0];
      f[i1].y += f1[1];
      f[i1].z += f1[2];
    }

    if (NEWTON_BOND || i2 < nlocal) {
      f[i2].x -= f1[0] + f3[0];
      f[i2].y -= f1[1] + f3[1];
      f[i2].z -= f1[2] + f3[2];
    }

    if (NEWTON_BOND || i3 < nlocal) {
      f[i3].x += f3[0];
      f[i3].y += f3[1];
      f[i3].z += f3[2];
    }

    if (EVFLAG) thr->ev_tally_thr( this, i1, i2, i3, nlocal, NEWTON_BOND, eangle, f1, f3,
                                   delx1, dely1, delz1, delx2, dely2, delz2 );
  }
}
