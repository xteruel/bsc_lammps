/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <math.h>
#include "pair_lj_cut_ompss.h"
#include "atom.h"
#include "comm.h"
#include "force.h"
#include "neighbor.h"
#include "neigh_list.h"

#include "accelerator_ompss.h"

#include "suffix.h"
using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

PairLJCutOMPSS::PairLJCutOMPSS(LAMMPS *lmp)
  : PairLJCut(lmp), ThrOMPSS(lmp)
{
  suffix_flag |= Suffix::OMPSS;
}

/* ---------------------------------------------------------------------- */

void PairLJCutOMPSS::compute(int eflag, int vflag)
{
  //MPI_Barrier( MPI_COMM_WORLD );
  //double t1 = MPI_Wtime( );
  if (eflag || vflag) {
    ev_setup(eflag,vflag);
  } else evflag = vflag_fdotr = 0;


  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none) \
  shared(eflag,vflag)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if   _OMPSS == 1
#   pragma omp task default(none) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    firstprivate(t) shared(eflag,vflag)
#   elif _OMPSS == 2
#   pragma oss task default(none) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    firstprivate(t) shared(eflag,vflag)
#   endif
    {
      ThrOMPSS thr;
      thr.init( (Pair *)this, ompss->shared_mem ? 0 : atom->nmax );
      if (evflag) {
        if (eflag) {
          if (force->newton_pair) eval<1,1,1>( t, &thr );
          else eval<1,1,0>( t, &thr );
        } else {
          if (force->newton_pair) eval<1,0,1>( t, &thr );
          else eval<1,0,0>( t, &thr );
        }
      } else {
        if (force->newton_pair) eval<0,0,1>( t, &thr );
        else eval<0,0,0>( t, &thr );
      }
      thr.reduce_thr( this, eflag, vflag );
    }
  }
  reduce_arrays( this );
}

/* ---------------------------------------------------------------------- */

template <int EVFLAG, int EFLAG, int NEWTON_PAIR>
void PairLJCutOMPSS::eval( int t, ThrOMPSS *thr )
{
  const dbl3_t * _noalias const x = (dbl3_t *) atom->x[0];
  dbl3_t * _noalias const f = (dbl3_t *) ompss->f[t][0];
  const int * _noalias const type = atom->type;
  const double * _noalias const special_lj = force->special_lj;
  const int * _noalias const ilist = ompss->t_list[t];
  const int * _noalias const numneigh = list->numneigh;
  const int * const * const firstneigh = list->firstneigh;

  double xtmp,ytmp,ztmp,delx,dely,delz,fxtmp,fytmp,fztmp;
  double rsq,r2inv,r6inv,forcelj,factor_lj,evdwl,fpair;

  const int nlocal = atom->nlocal;
  int j,jj,jnum,jtype;

  evdwl = 0.0;

  // loop over neighbors of my atoms
  const int natoms = ompss->t_natoms[t];

  for (int ii = 0; ii < natoms; ++ii) {
    const int i = ilist[ii];
    const int itype = type[i];
    const int    * _noalias const jlist = firstneigh[i];
    const double * _noalias const cutsqi = cutsq[itype];
    const double * _noalias const offseti = offset[itype];
    const double * _noalias const lj1i = lj1[itype];
    const double * _noalias const lj2i = lj2[itype];
    const double * _noalias const lj3i = lj3[itype];
    const double * _noalias const lj4i = lj4[itype];

    xtmp = x[i].x;
    ytmp = x[i].y;
    ztmp = x[i].z;
    jnum = numneigh[i];
    fxtmp=fytmp=fztmp=0.0;

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      factor_lj = special_lj[sbmask(j)];
      j &= NEIGHMASK;

      delx = xtmp - x[j].x;
      dely = ytmp - x[j].y;
      delz = ztmp - x[j].z;
      rsq = delx*delx + dely*dely + delz*delz;
      jtype = type[j];

      if (rsq < cutsqi[jtype]) {
        r2inv = 1.0/rsq;
        r6inv = r2inv*r2inv*r2inv;
        forcelj = r6inv * (lj1i[jtype]*r6inv - lj2i[jtype]);
        fpair = factor_lj*forcelj*r2inv;

        fxtmp += delx*fpair;
        fytmp += dely*fpair;
        fztmp += delz*fpair;
        if (NEWTON_PAIR || j < nlocal) {
          f[j].x -= delx*fpair;
          f[j].y -= dely*fpair;
          f[j].z -= delz*fpair;
        }

        if (EFLAG) {
          evdwl = r6inv*(lj3i[jtype]*r6inv-lj4i[jtype]) - offseti[jtype];
          evdwl *= factor_lj;
        }

        if (EVFLAG) thr->ev_tally_thr( this, i, j, nlocal, NEWTON_PAIR,
                                       evdwl, 0.0, fpair, delx, dely, delz );
      }
    }
    f[i].x += fxtmp;
    f[i].y += fytmp;
    f[i].z += fztmp;
  }
}
