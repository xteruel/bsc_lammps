/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <string.h>
#include "npair_half_size_bin_newtoff_ompss.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "atom.h"
#include "atom_vec.h"
#include "my_page.h"
#include "error.h"
#include "comm.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

NPairHalfSizeBinNewtoffOmpss::NPairHalfSizeBinNewtoffOmpss(LAMMPS *lmp) :
  NPair(lmp) {}

/* ----------------------------------------------------------------------
   size particles
   binned neighbor list construction with partial Newton's 3rd law
   each owned atom i checks own bin and surrounding bins in non-Newton stencil
   pair stored once if i,j are both owned and i < j
   pair stored by me if j is ghost (also stored by proc owning j)
------------------------------------------------------------------------- */

void NPairHalfSizeBinNewtoffOmpss::build(NeighList *list)
{
  int ntask = comm->nworkers;
  int nlocal = (includegroup) ? atom->nfirst : atom->nlocal;
  int history = list->history;
  int mask_history = 3 << SBBITS;

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nlocal,list,history,mask_history)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,nlocal,list,history,mask_history)
# endif
  for (int t=0; t < ntask; t++)
  {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );

    // loop over each atom, storing neighbors

    double **x = atom->x;
    double *radius = atom->radius;
    int *type = atom->type;
    int *mask = atom->mask;
    tagint *molecule = atom->molecule;

    int *ilist = list->ilist;
    int *numneigh = list->numneigh;
    int **firstneigh = list->firstneigh;

    // each thread has its own page allocator
    MyPage<int> *ipage = new MyPage<int>( &(list->ipage[t]) );
    ipage->reset();

    for (int i = ini; i < end; i++) {
      int n = 0;
      int *neighptr = ipage->vget();

      double xtmp = x[i][0];
      double ytmp = x[i][1];
      double ztmp = x[i][2];
      double radi = radius[i];
      int ibin = atom2bin[i];

      // loop over all atoms in surrounding bins in stencil including self
      // only store pair if i < j
      // stores own/own pairs only once
      // stores own/ghost pairs on both procs

      for (int k = 0; k < nstencil; k++) {
        for (int j = binhead[ibin+stencil[k]]; j >= 0; j = bins[j]) {
          if (j <= i) continue;
          if (exclude && exclusion(i,j,type[i],type[j],mask,molecule)) continue;

          double delx = xtmp - x[j][0];
          double dely = ytmp - x[j][1];
          double delz = ztmp - x[j][2];
          double rsq = delx*delx + dely*dely + delz*delz;
          double radsum = radi + radius[j];
          double cutsq = (radsum+skin) * (radsum+skin);

          if (rsq <= cutsq) {
            if (history && rsq < radsum*radsum)
              neighptr[n++] = j ^ mask_history;
            else
              neighptr[n++] = j;
          }
        }
      }

      ilist[i] = i;
      firstneigh[i] = neighptr;
      numneigh[i] = n;
      ipage->vgot(n);
      if (ipage->status())
        error->one(FLERR,"Neighbor list overflow, boost neigh_modify one");
    }
    delete ipage;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  list->inum = nlocal;
}
