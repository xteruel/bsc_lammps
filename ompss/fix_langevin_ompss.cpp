/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Carolyn Phillips (U Mich), reservoir energy tally
                         Aidan Thompson (SNL) GJF formulation
------------------------------------------------------------------------- */

#include <mpi.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include "fix_langevin_ompss.h"
#include "math_extra.h"
#include "atom.h"
#include "atom_vec_ellipsoid.h"
#include "force.h"
#include "update.h"
#include "modify.h"
#include "compute.h"
#include "domain.h"
#include "region.h"
#include "respa.h"
#include "comm.h"
#include "input.h"
#include "variable.h"
#include "random_mars.h"
#include "memory.h"
#include "error.h"
#include "group.h"

using namespace LAMMPS_NS;
using namespace FixConst;

enum{NOBIAS,BIAS};
enum{CONSTANT,EQUAL,ATOM};

#define SINERTIA 0.4          // moment of inertia prefactor for sphere
#define EINERTIA 0.2          // moment of inertia prefactor for ellipsoid

/* ---------------------------------------------------------------------- */

FixLangevinOMPSS::FixLangevinOMPSS(LAMMPS *lmp, int narg, char **arg) :
  FixLangevin(lmp, narg, arg)
{
  int nt = comm->nworkers;
  random_p = new RanMars *[nt];
  for (int i=0; i< nt; i++)
    random_p[i] = new RanMars(lmp,seed + comm->me*nt + i);
  delete random;
  random = NULL;
}

/* ---------------------------------------------------------------------- */

FixLangevinOMPSS::~FixLangevinOMPSS()
{
  if (random_p) delete [] random_p;
}

/* ----------------------------------------------------------------------
   modify forces using one of the many Langevin styles
------------------------------------------------------------------------- */

#ifdef TEMPLATED_FIX_LANGEVIN
template < int Tp_TSTYLEATOM, int Tp_GJF, int Tp_TALLY,
           int Tp_BIAS, int Tp_RMASS, int Tp_ZERO >
void FixLangevinOMPSS::post_force_templated()
#else
void FixLangevinOMPSS::post_force_untemplated
  (int Tp_TSTYLEATOM, int Tp_GJF, int Tp_TALLY,
   int Tp_BIAS, int Tp_RMASS, int Tp_ZERO)
#endif
{
  int nlocal = atom->nlocal;
  double **f = atom->f;
  int *mask = atom->mask;
  // apply damping and thermostat to atoms in group

  // for Tp_TSTYLEATOM:
  //   use per-atom per-coord target temperature
  // for Tp_GJF:
  //   use Gronbech-Jensen/Farago algorithm
  //   else use regular algorithm
  // for Tp_TALLY:
  //   store drag plus random forces in flangevin[nlocal][3]
  // for Tp_BIAS:
  //   calculate temperature since some computes require temp
  //   computed on current nlocal atoms to remove bias
  //   test v = 0 since some computes mask non-participating atoms via v = 0
  //   and added force has extra term not multiplied by v = 0
  // for Tp_RMASS:
  //   use per-atom masses
  //   else use per-type masses
  // for Tp_ZERO:
  //   sum random force over all atoms in group
  //   subtract sum/count from each atom in group

  double fsum[3];
  bigint count;

  compute_target();

  if (Tp_ZERO) {
    fsum[0] = fsum[1] = fsum[2] = 0.0;
    count = group->count(igroup);
    if (count == 0)
      error->all(FLERR,"Cannot zero Langevin force of 0 atoms");
  }

  // reallocate flangevin if necessary

  if (Tp_TALLY) {
    if (atom->nmax > maxatom1) {
      memory->destroy(flangevin);
      maxatom1 = atom->nmax;
      memory->create(flangevin,maxatom1,3,"langevin:flangevin");
    }
    flangevin_allocated = 1;
  }

  if (Tp_BIAS) temperature->compute_scalar();

  int ntask = comm->nworkers;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:fsum) \
  shared(nlocal,ntask,atom,force,update,temperature,mask,f,\
         groupbit, tforce,t_period,ratio,gfactor1, \
         gfactor2,franprev,gjffac,flangevin)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:fsum) \
  shared(nlocal,ntask,atom,force,update,temperature,mask,f,\
         groupbit, tforce,t_period,ratio,gfactor1, \
         gfactor2,franprev,gjffac,flangevin)
# endif
  for (int t=0; t< ntask; t++) {
    int di = nlocal/ntask;
    int mo = nlocal%ntask;
    int ini = t*di + ( t < mo ? t : mo );
    int end = ini + di + ( t < mo ? 1 : 0 );

    double gamma1, gamma2;
    double **v = atom->v;
    double *rmass = atom->rmass;
    int *type = atom->type;
    double boltz = force->boltz;
    double mvv2e = force->mvv2e;
    double ftm2v = force->ftm2v;
    double dt = update->dt;

    double fdrag[3], fran[3];
    double ltsqrt = tsqrt;
    class RanMars *rand = random_p[t];

    for (int i = ini; i < end; i++) {
      if (mask[i] & groupbit) {
        if (Tp_TSTYLEATOM) ltsqrt = sqrt(tforce[i]);
        if (Tp_RMASS) {
          gamma1 = -rmass[i] / t_period / ftm2v;
          gamma2 = sqrt(rmass[i]) * sqrt(24.0*boltz/t_period/dt/mvv2e) / ftm2v;
          gamma1 *= 1.0/ratio[type[i]];
          gamma2 *= 1.0/sqrt(ratio[type[i]]) * ltsqrt;
        } else {
          gamma1 = gfactor1[type[i]];
          gamma2 = gfactor2[type[i]] * ltsqrt;
        }

        fran[0] = gamma2*(rand->uniform()-0.5);
        fran[1] = gamma2*(rand->uniform()-0.5);
        fran[2] = gamma2*(rand->uniform()-0.5);

        if (Tp_BIAS) {
          temperature->remove_bias(i,v[i]);
          fdrag[0] = gamma1*v[i][0];
          fdrag[1] = gamma1*v[i][1];
          fdrag[2] = gamma1*v[i][2];
          if (v[i][0] == 0.0) fran[0] = 0.0;
          if (v[i][1] == 0.0) fran[1] = 0.0;
          if (v[i][2] == 0.0) fran[2] = 0.0;
          temperature->restore_bias(i,v[i]);
        } else {
          fdrag[0] = gamma1*v[i][0];
          fdrag[1] = gamma1*v[i][1];
          fdrag[2] = gamma1*v[i][2];
        }

        if (Tp_GJF) {
          double fswap = 0.5*(fran[0]+franprev[i][0]);
          franprev[i][0] = fran[0];
          fran[0] = fswap;
          fswap = 0.5*(fran[1]+franprev[i][1]);
          franprev[i][1] = fran[1];
          fran[1] = fswap;
          fswap = 0.5*(fran[2]+franprev[i][2]);
          franprev[i][2] = fran[2];
          fran[2] = fswap;

          fdrag[0] *= gjffac;
          fdrag[1] *= gjffac;
          fdrag[2] *= gjffac;
          fran[0] *= gjffac;
          fran[1] *= gjffac;
          fran[2] *= gjffac;
          f[i][0] *= gjffac;
          f[i][1] *= gjffac;
          f[i][2] *= gjffac;
        }

        f[i][0] += fdrag[0] + fran[0];
        f[i][1] += fdrag[1] + fran[1];
        f[i][2] += fdrag[2] + fran[2];

        if (Tp_TALLY) {
          flangevin[i][0] = fdrag[0] + fran[0];
          flangevin[i][1] = fdrag[1] + fran[1];
          flangevin[i][2] = fdrag[2] + fran[2];
        }

        if (Tp_ZERO) {
          fsum[0] += fran[0];
          fsum[1] += fran[1];
          fsum[2] += fran[2];
        }
      }
    } // atom loop
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif

  // set total force to zero

  if (Tp_ZERO) {
    double fsumall[3];
    MPI_Allreduce(fsum,fsumall,3,MPI_DOUBLE,MPI_SUM,world);
    fsumall[0] /= count;
    fsumall[1] /= count;
    fsumall[2] /= count;
    for (int i = 0; i < nlocal; i++) {
      if (mask[i] & groupbit) {
        f[i][0] -= fsumall[0];
        f[i][1] -= fsumall[1];
        f[i][2] -= fsumall[2];
      }
    }
  }

  // thermostat omega and angmom

  if (oflag) omega_thermostat();
  if (ascale) angmom_thermostat();
}
