/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "bond_harmonic_ompss.h"
#include "atom.h"
#include "comm.h"
#include "force.h"
#include "neighbor.h"
#include "domain.h"
#include "accelerator_ompss.h"

#include <math.h>

#include "suffix.h"
using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

BondHarmonicOMPSS::BondHarmonicOMPSS(LAMMPS *lmp)
  : BondHarmonic(lmp), ThrOMPSS(lmp)
{
  suffix_flag |= Suffix::OMPSS;
}


/* ---------------------------------------------------------------------- */

void BondHarmonicOMPSS::compute(int eflag, int vflag)
{

  if (eflag || vflag) {
    ev_setup(eflag,vflag);
  } else evflag = 0;

  const int nall = atom->nlocal + atom->nghost;
  const int nthreads = comm->nthreads;
  int inum = neighbor->nbondlist;

  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none) \
  shared(eflag,vflag,inum)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if     _OMPSS == 1
#   pragma omp task no_copy_deps default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    shared(evflag,eflag,vflag,force,inum)
#   elif   _OMPSS == 2
#   pragma oss task no_copy_deps default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    shared(evflag,eflag,vflag,force,inum)
#   endif
    {

      if (inum > 0) {
        ThrOMPSS thr;
        thr.init( (Bond *)this, ompss->shared_mem ? 0 : atom->nmax );
        if (evflag) {
          if (eflag) {
            if (force->newton_bond) eval<1,1,1>( t, &thr );
            else                    eval<1,1,0>( t, &thr );
          } else {
            if (force->newton_bond) eval<1,0,1>( t, &thr );
            else                    eval<1,0,0>( t, &thr );
          }
        } else {
          if (force->newton_bond) eval<0,0,1>( t, &thr );
          else                    eval<0,0,0>( t, &thr );
        }
        thr.reduce_thr( this, eflag, vflag );
      }
    } // end of omp parallel region
  }
  reduce_arrays( this );
}

template <int EVFLAG, int EFLAG, int NEWTON_BOND>
void BondHarmonicOMPSS::eval( int t, ThrOMPSS * const thr )
{
  int i1,i2,n,type;
  double delx,dely,delz,ebond,fbond;
  double rsq,r,dr,rk;

  const dbl3_t * _noalias const x = (dbl3_t *) atom->x[0];
  dbl3_t * _noalias const f = (dbl3_t *) ompss->f[t][0];
  const int * const * const bondlist = ompss->t_bondlist[t];
  const int nlocal = atom->nlocal;
  ebond = 0.0;

  const int nbond = ompss->t_nbondlist[t];
  for (n = 0; n < nbond; n++) {
    i1   = bondlist[n][0];
    i2   = bondlist[n][1];
    type = bondlist[n][2];

    delx = x[i1].x - x[i2].x;
    dely = x[i1].y - x[i2].y;
    delz = x[i1].z - x[i2].z;

    rsq = delx*delx + dely*dely + delz*delz;
    r = sqrt(rsq);
    dr = r - r0[type];
    rk = k[type] * dr;

    // force & energy

    if (r > 0.0) fbond = -2.0*rk/r;
    else fbond = 0.0;

    if (EFLAG) ebond = rk*dr;

    // apply force to each of 2 atoms

    if (NEWTON_BOND || i1 < nlocal) {
      f[i1].x += delx*fbond;
      f[i1].y += dely*fbond;
      f[i1].z += delz*fbond;
    }

    if (NEWTON_BOND || i2 < nlocal) {
      f[i2].x -= delx*fbond;
      f[i2].y -= dely*fbond;
      f[i2].z -= delz*fbond;
    }

    if (EVFLAG) thr->ev_tally_thr( this, i1, i2, nlocal, NEWTON_BOND,
                                   ebond, fbond, delx, dely, delz );
  }
}
