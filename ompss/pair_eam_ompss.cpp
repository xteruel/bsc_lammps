/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <math.h>
#include <string.h>

#include "pair_eam_ompss.h"
#include "atom.h"
#include "comm.h"
#include "force.h"
#include "memory.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "accelerator_ompss.h"

#include "suffix.h"
using namespace LAMMPS_NS;

#define MAXLINE 1024


/* ---------------------------------------------------------------------- */

PairEAMOMPSS::PairEAMOMPSS(LAMMPS *lmp)
  : PairEAM(lmp), ThrOMPSS(lmp)
{
  suffix_flag |= Suffix::OMPSS;
}

/* ---------------------------------------------------------------------- */

void PairEAMOMPSS::compute(int eflag, int vflag)
{
  if (eflag || vflag) {
    ev_setup(eflag,vflag);
  } else evflag = vflag_fdotr = eflag_global = eflag_atom = 0;

  int nlocal = atom->nlocal;
  const int nghost = atom->nghost;
  const int nall = nlocal + nghost;
  const int nthreads = comm->nthreads;
  int nworkers = comm->nworkers;
  const int inum = list->inum;

  // grow energy and fp arrays if necessary
  // need to be atom->nmax in length

  if (atom->nmax > nmax) {
    memory->destroy(rho);
    memory->destroy(fp);
    nmax = atom->nmax;
    memory->create(rho,nthreads*nmax,"pair:rho");
    memory->create(fp,nmax,"pair:fp");
  }
  reset_arr_thr( comm->nworkers, rho,
                 force->newton_pair ? nall  : nlocal,
                 ompss->shared_mem ? 0 : nmax );

  int *centinel = ompss->centinel;
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if _OMPSS == 1
#   pragma omp task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
# 	elif _OMPSS == 2
#   pragma oss task default(none) firstprivate(t) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } )
# 	endif
    {
      double *rho_t = ompss->shared_mem ? rho : rho + t*nmax;
      if (force->newton_pair) eval_rho<1>( t, rho_t );
      else eval_rho<0>( t, rho_t );
    }
  }
# if _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif

  if (!ompss->shared_mem) reduce_arr_thr( rho, 1 );

  // communicate and sum densities
  if (force->newton_pair) comm->reverse_comm_pair(this);

# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none) shared(nlocal,nworkers,eflag,vflag)
# endif
  for (int t=0; t< nworkers; t++) {
#   if   _OMPSS == 1
#   pragma omp task no_copy_deps default(none) \
    firstprivate(t) shared(nlocal,nworkers,eflag,vflag)
#   elif _OMPSS == 2
#   pragma oss task no_copy_deps default(none) \
    firstprivate(t) shared(nlocal,nworkers,eflag,vflag)
#   endif
    {
      int di = nlocal/nworkers;
      int mo = nlocal%nworkers;
      ThrOMPSS thr;
      thr.init( (Pair *)this, ompss->shared_mem ? 0 : atom->nmax );
      int ini = t*di + ( t < mo ? t : mo );
      int siz = di + ( t < mo ? 1 : 0 );
      if (force->newton_pair)
        if (eflag) eval_fp<1,1>( ini, ini+siz, &thr );
        else       eval_fp<0,1>( ini, ini+siz, &thr );
      else
        if (eflag) eval_fp<1,0>( ini, ini+siz, &thr );
        else       eval_fp<0,0>( ini, ini+siz, &thr );
      thr.reduce_thr( this, eflag, vflag );
    }
  }
# if   _OMPSS == 1
# pragma omp taskwait
# elif _OMPSS == 2
# pragma oss taskwait
# endif

  // communicate derivative of embedding function
  comm->forward_comm_pair(this);

  // compute forces
# if defined(_OPENMP) && !defined(_OMPSS)
# pragma omp parallel for default(none) shared(eflag,vflag)
# endif
  for (int tt=0; tt< ompss->GetNTask( ); tt++ ) {
    int        t = ompss->GetTask( tt );
    int  nneight = ompss->nneight[t];
    int *pneight = ompss->pneight[t];
#   if   _OMPSS == 1
#   pragma omp task default(none) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    firstprivate(t) shared(eflag,vflag)
#   elif _OMPSS == 2
#   pragma oss task default(none) \
    commutative( { centinel[pneight[ind]], ind=0;nneight } ) \
    firstprivate(t) shared(eflag,vflag)
#endif
    {
      ThrOMPSS thr;
      thr.init( (Pair *)this, ompss->shared_mem ? 0 : atom->nmax );

      if (evflag) {
        if (eflag) {
          if (force->newton_pair) eval<1,1,1>( t, &thr );
          else                    eval<1,1,0>( t, &thr );
        } else {
          if (force->newton_pair) eval<1,0,1>( t, &thr );
          else                    eval<1,0,0>( t, &thr );

        }
      } else {
        if (force->newton_pair) eval<0,0,1>( t, &thr );
        else                    eval<0,0,0>( t, &thr );
      }
      thr.reduce_thr( this, eflag, vflag );
    }
  }
  reduce_arrays( this );
}

template <int NEWTON_PAIR>
void PairEAMOMPSS::eval_rho( int t, double *rho_t )
{
  const dbl3_t * _noalias const x = (dbl3_t *) atom->x[0];
  const int    * _noalias const ilist = ompss->t_list[t];
  const int    * _noalias const numneigh = list->numneigh;
  const int    * _noalias const type = atom->type;
  const int * const * const firstneigh = list->firstneigh;
  const int nlocal = atom->nlocal;
  double xtmp, ytmp, ztmp, delx, dely, delz;
  double rsq, p;
  double *coeff;
  int    m, jj, j, jnum, jtype;
  const int natoms = ompss->t_natoms[t];
  for (int ii = 0; ii < natoms; ++ii) {
    const int i = ilist[ii];
    const int itype = type[i];
    const int    * _noalias const jlist = firstneigh[i];
    xtmp = x[i].x;
    ytmp = x[i].y;
    ztmp = x[i].z;
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      j &= NEIGHMASK;

      delx = xtmp - x[j].x;
      dely = ytmp - x[j].y;
      delz = ztmp - x[j].z;
      rsq = delx*delx + dely*dely + delz*delz;

      if (rsq < cutforcesq) {
        jtype = type[j];
        p = sqrt(rsq)*rdr + 1.0;
        m = static_cast<int> (p);
        m = MIN(m,nr-1);
        p -= m;
        p = MIN(p,1.0);
        coeff = rhor_spline[type2rhor[jtype][itype]][m];
        rho_t[i] += ((coeff[3]*p + coeff[4])*p + coeff[5])*p + coeff[6];
        if (NEWTON_PAIR || j < nlocal) {
          coeff = rhor_spline[type2rhor[itype][jtype]][m];
          rho_t[j] += ((coeff[3]*p + coeff[4])*p + coeff[5])*p + coeff[6];
        }
      }
    }
  }
}

template <int EFLAG,int NEWTON_PAIR>
void PairEAMOMPSS::eval_fp( const int iifrom, const int iito, ThrOMPSS *thr )
{
  int i, ii, m;
  double p, phi;
  const int * _noalias const ilist = list->ilist;
  const int * _noalias const type = atom->type;
  const int nlocal = atom->nlocal;
  double *coeff;
  // fp = derivative of embedding energy at each atom
  // phi = embedding energy at each atom
  // if rho > rhomax (e.g. due to close approach of two atoms),
  //   will exceed table, so add linear term to conserve energy
  for (ii = iifrom; ii < iito; ii++) {
    i = ilist[ii];
    p = rho[i]*rdrho + 1.0;
    m = static_cast<int> (p);
    m = MAX(1,MIN(m,nrho-1));
    p -= m;
    p = MIN(p,1.0);
    coeff = frho_spline[type2frho[type[i]]][m];
    fp[i] = (coeff[0]*p + coeff[1])*p + coeff[2];
    if (EFLAG) {
      phi = ((coeff[3]*p + coeff[4])*p + coeff[5])*p + coeff[6];
      if (rho[i] > rhomax) phi += fp[i] * (rho[i]-rhomax);
      thr->e_tally_thr( this, i, i, nlocal, NEWTON_PAIR, scale[type[i]][type[i]]*phi, 0.0 );
    }
  }
}

template <int EVFLAG, int EFLAG, int NEWTON_PAIR>
void PairEAMOMPSS::eval( const int t, ThrOMPSS * const thr )
{
  const dbl3_t * _noalias const x = (dbl3_t *) atom->x[0];
  dbl3_t * _noalias const f = (dbl3_t *) ompss->f[t][0];
  const int * _noalias const type = atom->type;
  const int * _noalias const ilist = ompss->t_list[t];
  const int * _noalias const numneigh = list->numneigh;
  const int * const * const firstneigh = list->firstneigh;
  const int nlocal = atom->nlocal;

  double xtmp,ytmp,ztmp,delx,dely,delz,fxtmp,fytmp,fztmp,fpair;
  double r,p,rsq,rhoip,rhojp,z2,z2p,recip,phip,psip,phi;
  int j,jj,jnum,jtype,m;
  double *coeff;
  double evdwl = 0.0;
  // compute forces on each atom
  // loop over neighbors of my atoms
  const int natoms = ompss->t_natoms[t];
  for (int ii = 0; ii < natoms; ii++) {
    const int i = ilist[ii];
    const int itype = type[i];
    xtmp = x[i].x;
    ytmp = x[i].y;
    ztmp = x[i].z;
    fxtmp = fytmp = fztmp = 0.0;

    const double * _noalias const scale_i = scale[itype];
    const int    * _noalias const jlist = firstneigh[i];
    jnum = numneigh[i];

    for (jj = 0; jj < jnum; jj++) {
      j = jlist[jj];
      j &= NEIGHMASK;

      delx = xtmp - x[j].x;
      dely = ytmp - x[j].y;
      delz = ztmp - x[j].z;
      rsq = delx*delx + dely*dely + delz*delz;

      if (rsq < cutforcesq) {
        jtype = type[j];
        r = sqrt(rsq);
        p = r*rdr + 1.0;
        m = static_cast<int> (p);
        m = MIN(m,nr-1);
        p -= m;
        p = MIN(p,1.0);

        // rhoip = derivative of (density at atom j due to atom i)
        // rhojp = derivative of (density at atom i due to atom j)
        // phi = pair potential energy
        // phip = phi'
        // z2 = phi * r
        // z2p = (phi * r)' = (phi' r) + phi
        // psip needs both fp[i] and fp[j] terms since r_ij appears in two
        //   terms of embed eng: Fi(sum rho_ij) and Fj(sum rho_ji)
        //   hence embed' = Fi(sum rho_ij) rhojp + Fj(sum rho_ji) rhoip

        coeff = rhor_spline[type2rhor[itype][jtype]][m];
        rhoip = (coeff[0]*p + coeff[1])*p + coeff[2];
        coeff = rhor_spline[type2rhor[jtype][itype]][m];
        rhojp = (coeff[0]*p + coeff[1])*p + coeff[2];
        coeff = z2r_spline[type2z2r[itype][jtype]][m];
        z2p = (coeff[0]*p + coeff[1])*p + coeff[2];
        z2 = ((coeff[3]*p + coeff[4])*p + coeff[5])*p + coeff[6];

        recip = 1.0/r;
        phi = z2*recip;
        phip = z2p*recip - phi*recip;
        psip = fp[i]*rhojp + fp[j]*rhoip + phip;
        fpair = -scale_i[jtype]*psip*recip;

        fxtmp += delx*fpair;
        fytmp += dely*fpair;
        fztmp += delz*fpair;
        if (NEWTON_PAIR || j < nlocal) {
          f[j].x -= delx*fpair;
          f[j].y -= dely*fpair;
          f[j].z -= delz*fpair;
        }

        if (EFLAG) evdwl = scale_i[jtype]*phi;
        if (EVFLAG) thr->ev_tally_thr( this, i, j, nlocal, NEWTON_PAIR,
                                       evdwl, 0.0, fpair, delx, dely, delz );

      }
    }
    f[i].x += fxtmp;
    f[i].y += fytmp;
    f[i].z += fztmp;
  }
}
