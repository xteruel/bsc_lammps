/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef DIHEDRAL_CLASS

DihedralStyle(charmm/ompss,DihedralCharmmOMPSS)

#else

#ifndef LMP_DIHEDRAL_CHARMM_OMPSS_H
#define LMP_DIHEDRAL_CHARMM_OMPSS_H

#include "dihedral_charmm.h"
#include "thr_ompss.h"

namespace LAMMPS_NS {

class DihedralCharmmOMPSS : public DihedralCharmm, public ThrOMPSS {
 public:
  DihedralCharmmOMPSS(class LAMMPS *);
  virtual void compute(int, int);

 private:
  template <int EVFLAG, int EFLAG, int NEWTON_BOND>
  void eval( int, ThrOMPSS * const, ThrOMPSS * const );
};

}

#endif
#endif
