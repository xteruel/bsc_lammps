/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(OMPSS,FixOMPSS)
#else

#ifndef LMP_FIX_OMPSS_H
#define LMP_FIX_OMPSS_H

#include "fix.h"


namespace LAMMPS_NS {

class ThrData;

class FixOMPSS : public Fix {
  friend class ThrOMPSS;
 public:
  FixOMPSS(class LAMMPS *, int, char **);
  virtual ~FixOMPSS();
  virtual int setmask();

  void setThrOMPSS( class ThrOMPSS *_thr ) { thr = _thr; }

  virtual void init();
  virtual void pre_force(int);
  virtual void setup_pre_force(int vflag)           { pre_force(vflag); }
  virtual void min_setup_pre_force(int vflag)       { pre_force(vflag); }
  virtual void min_pre_force(int vflag)             { pre_force(vflag); }
  virtual void setup_pre_force_respa(int vflag,int) { pre_force(vflag); }
  virtual void pre_force_respa(int vflag,int,int)   { pre_force(vflag); }

 protected:
  void *last_ompss_style;    // pointer to the style that needs
                           // to do the general force reduction
  void *last_pair_hybrid;  // pointer to the pair style that needs
                           // to call virial_fdot_compute()
 private:
  class ThrOMPSS *thr;
  bool _reset;    // whether forces have been reset for this step
  void set_neighbor_ompss();
};

}

#endif
#endif
