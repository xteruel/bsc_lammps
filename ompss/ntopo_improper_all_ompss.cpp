/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <mpi.h>
#include "ntopo_improper_all_ompss.h"
#include "atom.h"
#include "force.h"
#include "domain.h"
#include "update.h"
#include "output.h"
#include "thermo.h"
#include "memory.h"
#include "error.h"
#include "comm.h"

using namespace LAMMPS_NS;

#define DELTA 10000

/* ---------------------------------------------------------------------- */

NTopoImproperAllOmpss::NTopoImproperAllOmpss(LAMMPS *lmp) : NTopo(lmp)
{
  allocate_improper();
  coun = NULL;
  acum = NULL;
  memory->create(coun,comm->nworkers,"NTopoImproperAllOmpss:coun");
  memory->create(acum,comm->nworkers,"NTopoImproperAllOmpss:acum");
}

/* ---------------------------------------------------------------------- */

NTopoImproperAllOmpss::~NTopoImproperAllOmpss( )
{
  memory->destroy(acum);
  memory->destroy(coun);
}

/* ---------------------------------------------------------------------- */

void NTopoImproperAllOmpss::build()
{
  int ntask = comm->nworkers;
  int nlocal = atom->nlocal;

  int *num_improper = atom->num_improper;
  int **improper_type = atom->improper_type;
  tagint **improper_atom1 = atom->improper_atom1;
  tagint **improper_atom2 = atom->improper_atom2;
  tagint **improper_atom3 = atom->improper_atom3;
  tagint **improper_atom4 = atom->improper_atom4;
  int newton_bond = force->newton_bond;
  int lostbond = output->thermo->lostbond;

  int nmissing = 0;
# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_improper,improper_type, \
  improper_atom1,improper_atom2,improper_atom3,improper_atom4,lostbond, \
  newton_bond)
# elif _OMPSS == 2
# pragma oss task for default(none) reduction(+:nmissing) \
  shared(ntask,nlocal,num_improper,improper_type, \
  improper_atom1,improper_atom2,improper_atom3,improper_atom4,lostbond, \
  newton_bond)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_nimpr = 0;
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_improper[i]; m++) {
        int atom1 = atom->map(improper_atom1[i][m]);
        int atom2 = atom->map(improper_atom2[i][m]);
        int atom3 = atom->map(improper_atom3[i][m]);
        int atom4 = atom->map(improper_atom4[i][m]);
        if (atom1 == -1 || atom2 == -1 || atom3 == -1 || atom4 == -1) {
          nmissing++;
          if (lostbond == ERROR) {
            char str[128];
            sprintf(str,"Improper atoms "
                    TAGINT_FORMAT " " TAGINT_FORMAT " "
                    TAGINT_FORMAT " " TAGINT_FORMAT
                    " missing on proc %d at step " BIGINT_FORMAT,
                    improper_atom1[i][m],improper_atom2[i][m],
                    improper_atom3[i][m],improper_atom4[i][m],
                    me,update->ntimestep);
            error->one(FLERR,str);
          }
          continue;
        }
        atom1 = domain->closest_image(i,atom1);
        atom2 = domain->closest_image(i,atom2);
        atom3 = domain->closest_image(i,atom3);
        atom4 = domain-> closest_image(i,atom4);
        if (newton_bond ||
            (i <= atom1 && i <= atom2 && i <= atom3 && i <= atom4)) th_nimpr++;
      }
    }
    coun[t] = th_nimpr;
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  nimproperlist = 0;
  for (int t=0; t< ntask; t++) {
    acum[t] = nimproperlist;
    nimproperlist+=coun[t];
  }
  if (nimproperlist > maximproper) {
    maximproper = DELTA*(nimproperlist/DELTA) + DELTA;
    memory->grow(improperlist,maximproper,5,"neigh_topo:improperlist");
  }

# if _OMPSS == 1 || defined(_OPENMP)
# pragma omp parallel for default(none) \
  shared(ntask,nlocal,num_improper,improper_type, \
  improper_atom1,improper_atom2,improper_atom3,improper_atom4,newton_bond)
# elif _OMPSS == 2
# pragma oss task for default(none) \
  shared(ntask,nlocal,num_improper,improper_type, \
  improper_atom1,improper_atom2,improper_atom3,improper_atom4,newton_bond)
# endif
  for (int t=0; t< ntask; t++) {
    const int di = nlocal/ntask;
    const int mo = nlocal%ntask;
    const int ini = t*di + ( t < mo ? t : mo );
    const int end = ini + di + ( t < mo ? 1 : 0 );
    int th_ndihe = acum[t];
    for (int i=ini; i< end; i++) {
      for (int m = 0; m < num_improper[i]; m++) {
        int atom1 = atom->map(improper_atom1[i][m]);
        int atom2 = atom->map(improper_atom2[i][m]);
        int atom3 = atom->map(improper_atom3[i][m]);
        int atom4 = atom->map(improper_atom4[i][m]);
        if (atom1 == -1 || atom2 == -1 || atom3 == -1 || atom4 == -1) continue;
        atom1 = domain->closest_image(i,atom1);
        atom2 = domain->closest_image(i,atom2);
        atom3 = domain->closest_image(i,atom3);
        atom4 = domain-> closest_image(i,atom4);
        if (newton_bond ||
            (i <= atom1 && i <= atom2 && i <= atom3 && i <= atom4)) {
          improperlist[th_ndihe][0] = atom1;
          improperlist[th_ndihe][1] = atom2;
          improperlist[th_ndihe][2] = atom3;
          improperlist[th_ndihe][3] = atom4;
          improperlist[th_ndihe][4] = improper_type[i][m];
          th_ndihe++;
        }
      }
    }
  }
# if _OMPSS == 2
# pragma oss taskwait
# endif
  if (cluster_check) dihedral_check(nimproperlist,improperlist);
  if (lostbond == IGNORE) return;

  int all;
  MPI_Allreduce(&nmissing,&all,1,MPI_INT,MPI_SUM,world);
  if (all) {
    char str[128];
    sprintf(str,
            "Improper atoms missing at step " BIGINT_FORMAT,update->ntimestep);
    if (me == 0) error->warning(FLERR,str);
  }
}
