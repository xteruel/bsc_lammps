/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifdef PAIR_CLASS

PairStyle(lj/cut/ompss,PairLJCutOMPSS)

#else

#ifndef LMP_PAIR_LJ_CUT_OMPSS_H
#define LMP_PAIR_LJ_CUT_OMPSS_H

#include "pair_lj_cut.h"
#include "thr_ompss.h"

namespace LAMMPS_NS {

class PairLJCutOMPSS : public PairLJCut, public ThrOMPSS {
 public:
  PairLJCutOMPSS(class LAMMPS *);
  virtual void compute(int, int);

 private:

  template <int EVFLAG, int EFLAG, int NEWTON_PAIR>
  void eval( int t, ThrOMPSS *thr );
};

}

#endif
#endif
