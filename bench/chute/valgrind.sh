#!/bin/bash
SCALE=10
NPROC=6; [ $# -ge 1 ] && [ "$1" -eq "$1" ] &> /dev/null && [ $1 -gt 0 ] && NPROC=$1
NTHRE=8; [ $# -ge 2 ] && [ "$2" -eq "$2" ] &> /dev/null && [ $2 -gt 0 ] && NTHRE=$2
InputF=in.chute.scaled
INputA="data.chute"
scriptF=./src.sh
txt=$(<${InputF})

bdir=$(realpath ./)
tdir=$(realpath ../)

wdir=Valgrind
[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}; cd ${wdir}

#cp ${bdir}/${InputF} .
echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${InputF}
for f in ${INputA}; do cp ${bdir}/${f} .; done

if true; then
    EXE=${tdir}/lmp_ompss
    #VFLAGS=--leak-check=full -v --track-origins=yes
    VFLAGS="--leak-check=full"
    src="#!/bin/bash
    export OMP_NUM_THREADS=${NTHRE}
    export NX_ARGS=\"--smp-workers=${NTHRE} --summary --disable-ut\"
    valgrind ${VFLAGS} ${EXE} -i ${InputF}"
    echo "${src}" > ${scriptF}
    chmod +x ${scriptF}
    #MPI_FLAG="--map-by socket:pe=${NTHRE}"
    mpirun ${MPI_FLAG} -np ${NPROC} ${scriptF} 2>&1 | tee kk1
    #mpirun -np ${NPROC} ${scriptF} > kk1
    # | tee kk1
fi

if false; then
    EXE=${tdir}/lmp_omp
    src="#!/bin/bash
    export OMP_NUM_THREADS=${NTHRE}
    ${EXE} -sf omp -pk omp ${NTHRE} -i ${InputF}"
#    ${EXE} -i ${InputF}"
    echo "${src}" > ${scriptF}
    chmod +x ${scriptF}
    mpirun -np ${NPROC} ${scriptF} | tee kk2
fi

if false; then
    EXE=${tdir}/lmp_kokkos
    src="#!/bin/bash
    export OMP_NUM_THREADS=${NTHRE}
    ${EXE} -sf kk -k on t ${NTHRE} -i ${InputF}"
    echo "${src}" > ${scriptF}
    chmod +x ${scriptF}
    mpirun -np ${NPROC} ${scriptF} > kk3
fi
