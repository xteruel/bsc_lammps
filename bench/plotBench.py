#!/usr/bin/env python
from os.path  import isdir, join, isfile, basename
from os       import listdir, getcwd

import numpy as np
import matplotlib.pyplot as plt
import locale
locale.setlocale(locale.LC_ALL, 'en_US')

BASIC=False
TOTAL=True

PNAME = basename( getcwd() ).upper()  # capitalize()
WDIR    = "TEST"
st_tags = [ "omp", "ompss" ] # "kokkos"
plot_bx = [ 121, 122 ]
ax      = [ None, None ]

SHOW=False
OFILE   = "output"
#APP=[ "lj", "cycle" ]
APP=[ "cycle" ]

SCA=2.5
FIGSIZE = [ SCA*6.4, SCA*4.8 ]
#COLORS  = [ "b", "r", "g" ]
COLORS1={ "OMP": "red", "OMPSS": "green" }
COLORS2={ "OMP": "tomato", "OMPSS": "lawngreen" }
fntsiz1 = 18
fntsiz2 = 16
fntsiz3 = 12

plt.rcParams.update({'font.size': 12})

if not isdir(WDIR) : exit()

sdirs= [ d for d in listdir(WDIR) if isdir(join(WDIR,d)) ]
scales={ }

for d in sorted( sdirs, key = lambda x: ( int(x.split("_")[0]), int(x.split("_")[1]) ) ) :
    sc = int(d.split("_")[0])
    if sc not in scales :
        scales[sc] = [ d ]
    else :
        scales[sc] = scales[sc] + [ d ]

if BASIC :
    for t in scales :
        dirs = scales[t]
        natoms=0
        GRAPH={}
        for d in dirs :
            NP, NT, mode = d.split("_")[1:]
            NP=int(NP)
            NT=int(NT)
            fn=join(WDIR,d,"output")
            if not isfile(fn) : exit( "Error: missing file '"+fn+"'" )
            lines=open(fn).readlines( )
            if not natoms :
                for l in lines :
                    if "Loop time" in l :
                        natoms = int(l.split()[-2] )
                        break
            t_reg=[ float(l.split("=")[1]) for l in lines if "REG TIME_cycle" in l ]
            t_reg=np.average( t_reg )
            t_reb=[ float(l.split("=")[1]) for l in lines if "REB TIME_cycle" in l ]
            print( fn, 'REB', t_reb )
            t_reb=np.average( t_reb )
            name = "%dp/%dt"%( NP, NT )
            if mode not in GRAPH :
                GRAPH[mode] = { "x": [ name ], "y": [ t_reg ], "z": [ t_reb ] }
            else :
                GRAPH[mode]["x"] = GRAPH[mode]["x"] + [ name ]
                GRAPH[mode]["y"] = GRAPH[mode]["y"] + [ t_reg ]
                GRAPH[mode]["z"] = GRAPH[mode]["z"] + [ t_reb ]
        print( GRAPH )
        #fig = plt.figure(figsize=FIGSIZE)
        fig = plt.figure( )
        for mode in GRAPH :
            x = np.array(GRAPH[mode]["x"][::-1])
            y = np.array(GRAPH[mode]["y"][::-1])
            z = np.array(GRAPH[mode]["z"][::-1])
            plt.plot( x, y, COLORS1[mode], label=mode )
            plt.plot( x, y, COLORS1[mode], marker='o' )
            plt.plot( x, z, COLORS2[mode], label=mode+" rebuild" )
            plt.plot( x, z, COLORS2[mode], marker='o' )
            plt.legend()

        plt.ylabel('Time', fontweight='bold', labelpad=1) # , fontsize=fntsiz2, labelpad=5
        plt.xlabel('Threads per Process', fontweight='bold') # fontsize=fntsiz2, labelpad=5
        plt.xticks()  # fontsize=fntsiz3
        plt.yticks()  # fontsize=fntsiz3
        fig.suptitle(PNAME+'('+f'{natoms:,}'+' atoms): Time/Iteration', fontweight='bold') # , fontsize=fntsiz1

        plt.ylim(bottom=0)
        plt.show()
        of="single_node_%02d.png"%( t )
        fig.savefig( of, dpi=100 )

if TOTAL :
    NITERS=None
    for t in scales :
        dirs = scales[t]
        natoms=0
        GRAPH={}
        for d in dirs :
            NP, NT, mode = d.split("_")[1:]
            NP=int(NP)
            NT=int(NT)
            fn=join(WDIR,d,"output")
            if not isfile(fn) : exit( "Error: missing file '"+fn+"'" )
            lines=open(fn).readlines( )
            if not natoms :
                for l in lines :
                    if "Loop time" in l :
                        natoms = int(l.split()[-2] )
                        break
            if not NITERS :
                NITERS=[ int(l.split()[8]) for l in lines if "Loop time" in l ][0]
            time=[ float(l.split("=")[1])/NITERS for l in lines if "TIME_total" in l ][0]
            name = "%dp/%dt"%( NP, NT )
            if mode not in GRAPH :
                GRAPH[mode] = { "x": [ name ], "y": [ time ] }
            else :
                GRAPH[mode]["x"] = GRAPH[mode]["x"] + [ name ]
                GRAPH[mode]["y"] = GRAPH[mode]["y"] + [ time ]
        print( GRAPH )
        #fig = plt.figure(figsize=FIGSIZE)
        fig = plt.figure( )
        for mode in GRAPH :
            x = np.array(GRAPH[mode]["x"][::-1])
            y = np.array(GRAPH[mode]["y"][::-1])
            plt.plot( x, y, COLORS1[mode], label=mode )
            plt.plot( x, y, COLORS1[mode], marker='o' )
            plt.legend()

        plt.ylabel('Time', fontweight='bold') # , fontsize=fntsiz2, labelpad=5
        plt.xlabel('Threads per Process', fontweight='bold') # fontsize=fntsiz2, labelpad=5
        plt.xticks()  # fontsize=fntsiz3
        plt.yticks()  # fontsize=fntsiz3
        fig.suptitle(PNAME+'('+f'{natoms:,}'+' atoms): Average time/Iteration', fontweight='bold') # , fontsize=fntsiz1

        plt.ylim(bottom=0)
        plt.subplots_adjust(left=0.15)
        plt.show()
        of="single_node_avg_%02d.png"%( t )
        fig.savefig( of, dpi=100 )

