#!/usr/bin/env python
from os.path  import isdir, join, isfile, basename
from os       import listdir, getcwd, getenv
from math     import log10, floor

import numpy as np
import matplotlib.pyplot as plt

PLOT= getenv("NOPLOT") == None
if PLOT : print( "*** export NOPLOT= to suppres plots" )

BASIC=True
STACK=False
PNAME = basename( getcwd() ).upper()  # capitalize()
WDIR  = "1Node"
ITERS = 25

SHOW=False
OFILE   = "output"
#APP=[ "lj", "cycle" ]
APP=[ "cycle" ]

SCA=2
FIGSIZE = [ SCA*6.4, SCA*4.8 ]
#COLORS  = [ "b", "r", "g" ]
COLORS={ "OMP": "b", "OMPSS": "r", "MPI": "g" }

#plt.rcParams.update({'font.size': 16})
#plt.rcParams.update({'font.weight': 'bold'})
fnthead = 24
fntsiz1 = 18
fntsiz2 = 16
fntsiz3 = 12

if not isdir(WDIR) : exit()

sdirs= [ d for d in listdir(WDIR) if isdir(join(WDIR,d)) ]

if BASIC :
    niters = None
    if isfile(".iters") :
        niters = [ int(i) for i in open(".iters").readlines() ]
        if len(niters) == 1 : niters = [ 0, niters[0] ]
    scales={ }
    for d in sorted( sdirs, key = lambda x: (x.split("_")[1],-int(x.split("_")[2]),x.split("_")[0])) :
        sc = d.split("_",1)[1]
        if sc not in scales :
            scales[sc] = [ d ]
        else :
            scales[sc] = scales[sc] + [ d ]
    for t in scales :
        dirs = scales[t]
        natoms=0
        GRAPH={}
        for d in dirs :
            print( "Process DIR", d )
            mode, sc, NP = d.split("_")
            NP=int(NP)
            SC=int(sc)
            NT=48/NP
            fn=join(WDIR,d,"output")
            if not isfile(fn) : exit( "Error: missing file '"+fn+"'" )
            lines=open(fn).readlines( )
            if not natoms :
                for l in lines :
                    if "Loop time" in l :
                        natoms = int(l.split()[-2] )
                        break
            times=[ float(l.split("=")[1]) for l in lines if "TIME_cycle" in l ]
            if not niters : niters = [ 0, len(times) ]
            if len(times)>niters[1] : times = times[niters[0]:niters[1]]
            GRAPH[mode] = { "x": [ i for i in range(len(times)) ], "y": times }
        fig = plt.figure(figsize=FIGSIZE)
        dinc = 0.3
        inc  = -dinc
        for mode in GRAPH :
            print( "Process GRAPH", mode )
            x = np.array(GRAPH[mode]["x"][::-1])
            y = np.array(GRAPH[mode]["y"][::-1])
            x = x + inc
    #        plt.plot( x, y, COLORS[mode], label=mode )
    #        plt.plot( x, y, COLORS[mode], marker='o' )
            plt.bar( x, y, dinc, alpha=0.9, color=COLORS[mode], label=mode)
            plt.legend()
            inc += dinc

        plt.ylabel('Time', fontsize=fntsiz2, labelpad=5, fontweight='bold')
        plt.xlabel('Iteration', fontsize=fntsiz2, labelpad=5, fontweight='bold')
        plt.xticks(fontsize=fntsiz3)
        plt.yticks(fontsize=fntsiz3)
        fig.suptitle('Time x Iteration ('+PNAME+' atoms=%d nthreads=%d)'%( natoms, NT ), fontsize=fntsiz1, fontweight='bold')

        plt.ylim(bottom=0)
        if PLOT : plt.show()
        of="sn_%d_%02d.png"%( SC, NT )
        fig.savefig( of, dpi=100 )



if STACK :
           #"DIST_CHECK": "DIST_CHECK",
           #"TOPOLOGY": "TOPOLOGY",
    DATA={ "PRE_EXCHANGE": "Pre Exchange",
           "COMM_MODE": "Communications",
           "BIN_ATOMS": "Bin Atoms",
           "NEIGH_PAIR": "Build Connectivity",
           "TOPOLOGY": "Topology",
           "POS_NEIGH": "Pos Neighbour",
           "OMPSS_BUILD": "OMPSS Build" }
    COLOR={ "COMM_MODE": 'C0',
            "BIN_ATOMS": 'C1',
            "NEIGH_PAIR": 'C2',
            "TOPOLOGY": 'C3',
            "POS_NEIGH": 'C4',
            "OMPSS_BUILD": 'C5',
            "PRE_EXCHANGE": 'C6' }
    scales={ }
    for d in sorted( sdirs, key = lambda x: (x.split("_")[1],-int(x.split("_")[2]),x.split("_")[0])) :
        sc = d.split("_")[1]
        if sc not in scales :
            scales[sc] = [ d ]
        else :
            scales[sc] = scales[sc] + [ d ]

    for t in scales :
        dirs = scales[t]
        natoms=0
        GRAPH={}
        for k in list(DATA.keys())+[ "tags" ] : GRAPH[k] = []
        print( dirs )
        x = None
        for d in dirs :
            print( "Process DIR", d )
            mode, sc, NP = d.split("_")
            if x is None :
                x = [ 0 ]
                first_x = mode
            else :
                x.append( x[-1]+1.5 if mode==first_x else x[-1]+1 )
            NP=int(NP)
            SC=int(sc)
            NT=48/NP
            fn=join(WDIR,d,"output")
            if not isfile(fn) : exit( "Error: missing file '"+fn+"'" )
            lines=open(fn).readlines( )
            if not natoms :
                for l in lines :
                    if "Loop time" in l :
                        natoms = int(l.split()[-2] )
                        break
            for k in DATA :
                a = []
                for l in lines :
                    if l.startswith(k) :
                        a.append( float(l.split("=")[-1]) )
                if not a        : a = 0.0
                elif len(a) < 6 : a = np.array( a ).mean()
                elif len(a) < 8 : a = np.array( sorted(a)[:-1] ).mean()
                else            : a = np.array( sorted(a)[1:-2] ).mean()
                GRAPH[k].append( 0.0 if a is None else a )
            GRAPH["tags"].append( mode + " %d"%( NT ) )
        fig = plt.figure(figsize=FIGSIZE)

        width=0.8
        dy=None
        x=np.array(x)
        for k in DATA :
            y=np.array(GRAPH[k])
            if dy is None : dy=np.zeros(y.size)
            plt.bar( x, y, width, bottom=dy, label=DATA[k], color=COLOR[k] )
            for ix, iy, v in zip(x,dy,y) :
                if v != 0.0 :
                    txt = ("%.4lf" if v>1e-3 else "%.2E")%v
                    plt.text( ix, iy+v/10, "-".join(txt.split("-0")), ha='center', fontsize=12 )
            dy = dy+y
        ymax = dy.max()*1.1
        ymax=round(ymax,1-int(floor(log10(abs(ymax)))))
        plt.ylabel('Time', fontsize=fntsiz2, labelpad=5, fontweight='bold')
        plt.ylim(top=ymax)
        xlabels = [ t.split()[0]+"\n"+t.split()[-1]+" threads" for t in GRAPH["tags"] ]
        plt.xticks(x, xlabels, fontweight='bold' ) # rotation='vertical'
        plt.yticks(fontweight='bold')
        handles, labels = fig.axes[0].get_legend_handles_labels()
        plt.legend( handles[::-1], labels[::-1], loc='upper left' )
        #ax = fix.axes[0]
        fig.suptitle("Time of neighbor list rebuild ("+PNAME+' %d atoms)'%( natoms ), fontsize=fnthead, fontweight='bold')
        if PLOT : plt.show()
        of="sn_rebuild_"+t+".png"
        fig.savefig( of, dpi=100 )
