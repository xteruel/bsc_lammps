#!/bin/bash

input_f="in.rhodo.scaled"
data_f="data.rhodo"
topdir=$(realpath ../../)
original_files=$( for f in ${input_f} ${data_f}; do realpath $f; done )

GNU=NO
TEST_DIR=WeakScaling
rm -rf ${TEST_DIR} &> /dev/null

if [ -d ${TEST_DIR} ]; then
  i=1
  tt=$( printf "${TEST_DIR}-%02d" "${i}" )
  while [ -d ${tt} ]; do i=$(( i+1 )); tt=$( printf "${TEST_DIR}-%02d" "${i}" ); done
  mv ${TEST_DIR} ${tt}
fi
mkdir ${TEST_DIR}; cd ${TEST_DIR}

for NNODES in 1 2 4 8 16 32 48 64 80 96; do
    QOS=
    [ ${NNODES} -ge 50 ] && QOS="#SBATCH --qos=xlarge"
    [ ${NNODES} -le 2 ] && QOS="#SBATCH --qos=debug"
    NCPUS=$(( NNODES*48 ))
    dir=$( printf "cpus_%04d" "${NCPUS}" )
    [ -d ${dir} ] && rm -rf ${dir}; mkdir ${dir}
    pushd ${dir} &> /dev/null
    factors=()
    n=${NCPUS}
    while [ "${n}" -gt 1 ]; do
        i=2
        while [ $(( $n -($n/$i)*i )) -ne 0 ]; do i=$(( i+1 )); done
        factors+=( $i )
        n=$(( $n/$i ))
    done
    esc=(1 1 1)
    for i in $( seq ${#factors[@]}); do
        v=${factors[-$i]}
        [[ ${esc[2]} -le ${esc[1]} && ${esc[2]} -le ${esc[0]} ]] && esc[2]=$(( ${esc[2]}*$v )) ||
            { [ ${esc[1]} -le ${esc[0]} ] && esc[1]=$(( ${esc[1]}*$v )) || esc[0]=$(( ${esc[0]}*$v )); }
    done
    [ ${esc[1]} -lt ${esc[0]} ] && { t=${esc[1]}; esc[1]=${esc[0]}; esc[0]=${t}; }
    [ ${esc[2]} -lt ${esc[1]} ] && { t=${esc[2]}; esc[2]=${esc[1]}; esc[1]=${t}; }
    [ ${esc[1]} -lt ${esc[0]} ] && { t=${esc[1]}; esc[1]=${esc[0]}; esc[0]=${t}; }
    cat << EOF > lammps.sh
#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS.out
#SBATCH --error=LAMMPS.err
#SBATCH --ntasks=$(( 48*${NNODES} ))
#SBATCH --cpus-per-task=1
#SBATCH --time=00:30:00
${QOS}
OMP="${topdir}/lmp_omp"
OMPSS="${topdir}/lmp_ompss"
KOKKOS="${topdir}/lmp_kokkos"
for threads in 1 6 8 12; do
    procs=\$(( ${NCPUS} / \${threads} ))
    export SLURM_CPUS_PER_TASK=\${threads}
    export SLURM_NPROCS=\${procs}
    export SLURM_NTASKS=\${procs}
    export SLURM_TASKS_PER_NODE="\$(( \${procs}/${NNODES} ))(x${NNODES})"
    for MOD in "OMPSS" "OMP" "KOKKOS"; do
        fn=\$( printf "\${MOD}-%04d-%02d" "\${procs}" "\${threads}" )
        mkdir \${fn}
        pushd \${fn} &> /dev/null
        cp $( echo ${original_files} ) .
        echo -e "%s/x index 1/x index ${esc[0]}/g\n" \
                "%s/y index 1/y index ${esc[1]}/g\n" \
                "%s/z index 1/z index ${esc[2]}/g\nw\nq\n" | ex ${input_f}
        script=\${fn}.sh
        EXEC=\${!MOD}
        EXTRA_FLAGS=""
        if [ \${threads} -gt 1 ]; then
            [ \${MOD} == "OMP" ]    && EXTRA_FLAGS="-sf omp -pk omp \${threads}"
            [ \${MOD} == "KOKKOS" ] && EXTRA_FLAGS="-k on t \${threads} -sf kk" && export OMP_PROC_BIND=true
        fi
cat << EOOF > \${script}
#!/bin/bash
export OMP_NUM_THREADS=\${threads}
\${EXEC} \${EXTRA_FLAGS} -i ${input_f}
EOOF
        chmod +x \${script}
        srun ./\${script} > output 2> error
        popd &> /dev/null
    done
done
EOF
        chmod +x lammps.sh
        sbatch lammps.sh
        popd &> /dev/null
done
