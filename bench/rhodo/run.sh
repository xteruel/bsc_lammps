#!/bin/bash

SCALE=5
TRACE=true
OMPSS=true
OMP=true
KOKKOS=false

DEBUG=false

wdir=work
IS_INTEL=true

NPROCS=6; [ $# -ge 1 ] && [ $1 -eq $1 ] &> /dev/null && [ $1 -gt 0 ] && NPROCS=$1
NTHREA=8; [ $# -ge 2 ] && [ $2 -eq $2 ] &> /dev/null && [ $2 -gt 0 ] && NTHREA=$2

input_f="in.rhodo.scaled"
data_f="data.rhodo"
topdir=$(realpath ../../)

pushd ${topdir}
if ${TRACE}; then
    make -j 48 TRACE=YES EXT=_prv || exit
else
    make -j 48 || exit
fi
popd

#[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}
[ -d ${wdir} ] && {
  i=1
  Wdir=$( printf "${wdir}.%03d" $i )
  while [ -d ${Wdir} ]; do i=$(( i+1 )); Wdir=$( printf "${wdir}.%03d" $i ); done
  mv ${wdir} ${Wdir}
  }
mkdir ${wdir}

cp ${input_f} ${data_f} extrae.xml ${wdir}

cd ${wdir}

txt=$(<${input_f})
echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${input_f}

script=./lammps.sh

if ${TRACE}; then
    if ${OMPSS}; then
        EXE="${topdir}/lmp_ompss_prv"
        EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
        src="#!/bin/bash
            export OMP_NUM_THREADS=${NTHREA}
            export LD_PRELOAD=${EXTRAE_HOME}/lib/libnanosmpitrace.so
            export EXTRAE_CONFIG_FILE=extrae.xml
            export NX_ARGS=\"--pes ${NTHREA} --instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state\"
            export NX_INSTRUMENTATION=extrae
            ${EXE} -pk ompss -sf ompss -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun ${MPI_FLAG} -np ${NPROCS} ${script} 2>&1 | tee kk1
    fi
    if ${OMP}; then
        EXE="${topdir}/lmp_omp_prv"
        EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
        src="#!/bin/bash
            export OMP_NUM_THREADS=${NTHREA}
            export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
            export EXTRAE_CONFIG_FILE=extrae.xml
            ${EXE} -sf omp -pk omp ${NTHREA} -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun ${MPI_FLAG} -np ${NPROCS} ${script} 2>&1 | tee kk2
    fi
    if ${KOKKOS}; then
        EXE="${topdir}/lmp_kokkos_prv"
        EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
        src="#!/bin/bash
            export OMP_NUM_THREADS=${NTHREA}
            export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
            export EXTRAE_CONFIG_FILE=extrae.xml
            ${EXE} -sf kk -k on t ${NTHREA} -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun ${MPI_FLAG} -np ${NPROCS} ${script} 2>&1 | tee kk3
    fi
else
    if ${OMPSS}; then
        EXE=${topdir}/lmp_ompss
        ${DEBUG} && EXE="valgrind ${EXE}"
        src="#!/bin/bash
        export OMP_NUM_THREADS=${NTHREA}
        export NX_ARGS=\"--smp-workers=${NTHREA} --summary --disable-ut\"
        ${EXE} -pk ompss -sf ompss -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        #MPI_FLAG="--map-by socket:pe=${NTHREA}"
        mpirun ${MPI_FLAG} -np ${NPROCS} ${script} 2>&1 | tee kk1
        #mpirun -np ${NPROCS} ${scriptF} > kk1
        # | tee kk1
    fi
    if ${OMP}; then
        EXE=${topdir}/lmp_omp
        src="#!/bin/bash
        export OMP_NUM_THREADS=${NTHREA}
        ${EXE} -sf omp -pk omp ${NTHREA} -i ${input_f}"
        #${EXE} -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun ${MPI_FLAG} -np ${NPROCS} ${script} 2>&1 | tee kk2
    fi
    if ${KOKKOS}; then
        EXE=${topdir}/lmp_kokkos
        src="#!/bin/bash
        export OMP_NUM_THREADS=${NTHREA}
        ${EXE} -sf kk -k on t ${NTHREA} -i ${input_f}"
        echo "${src}" > ${script}
        chmod +x ${script}
        mpirun ${MPI_FLAG} -np ${NPROCS} ${script} 2>&1 | tee kk3
    fi
fi
