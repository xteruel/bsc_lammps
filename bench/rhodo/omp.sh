#!/bin/bash

wdir=womp
IS_INTEL=true
TRACE=true

NPROCS=8; [ $# -ge 1 ] && [ $1 -eq $1 ] &> /dev/null && [ $1 -gt 0 ] && NPROCS=$1
NTHREA=6; [ $# -ge 2 ] && [ $2 -eq $2 ] &> /dev/null && [ $2 -gt 0 ] && NTHREA=$2

EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4

if [ -d ${wdir} ]; then
    i=1
    cdir=$( printf "${wdir}.%02d\n" $i )
    while [ -d ${cdir} ]; do
        i=$(( i + 1 ))
        cdir=$( printf "${wdir}.%02d\n" $i )
    done
    mv ${wdir} ${cdir}
fi
mkdir ${wdir}

input_f="in.rhodo.scaled"
data_f="data.rhodo"
topdir=$(realpath ../../)
CMEM=$(realpath checkmemory.sh)
EXE="${topdir}/lmp_omp"
${TRACE} && EXE=${EXE}_ext

cp ${input_f} ${data_f} extrae.xml ${wdir}

cd ${wdir}

${IS_INTEL} && HFILE_FORMAT=":" || HFILE_FORMAT=" slots="
if [ ! -z "${SLURM_NODELIST}" ]; then
    for line in $( scontrol show hostnames $SLURM_JOB_NODELIST ); do
        echo "$line${HFILE_FORMAT}48"
    done > hfile
else
    echo "localhost${HFILE_FORMAT}${NPROCS}" > hfile
fi

MSG=$( nm -a ${EXE} )
funcs="_ZN9LAMMPS_NS6Verlet3runEi
       _ZN9LAMMPS_NS6Verlet11force_clearEv
       _ZN9LAMMPS_NS23PairLJCharmmCoulLongOMP7computeEii
       _ZN9LAMMPS_NS15BondHarmonicOMP7computeEii
       _ZN9LAMMPS_NS14AngleCharmmOMP7computeEii
       _ZN9LAMMPS_NS17DihedralCharmmOMP7computeEii
       _ZN9LAMMPS_NS19ImproperHarmonicOMP7computeEii
       _ZN9LAMMPS_NS7PPPMOMP7computeEii
       _ZN9LAMMPS_NS6FixOMP9pre_forceEi
       "
for f in ${funcs}; do
    line=( $( echo "$MSG" | grep -w "${f}$" ) )
    echo "${line[0]}#${line[2]}" >> functions.dat
done

script=./lammps.sh

cat << EOF > ${script}
#!/bin/bash
export OMP_NUM_THREADS=${NTHREA}
if ${TRACE}; then
    export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
    export EXTRAE_CONFIG_FILE=extrae.xml
fi

${EXE} -sf omp -pk omp ${NTHREA} -i ${input_f}
EOF

chmod +x ${script}

${CMEM} meminfo .log &
mpirun -np ${NPROCS} --hostfile hfile ${script} 2>&1 | tee output
rm .log

[ -f $( basename ${EXE} ).prv ] && 
    /apps/BSCTOOLS/extrae/latest/impi_2017_4/bin/mpi2prv -f TRACE.mpits -o $( basename ${EXE} ).prv
