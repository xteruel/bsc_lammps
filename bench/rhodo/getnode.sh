#!/bin/bash
#SBATCH --job-name=compile
#SBATCH --workdir=.
#SBATCH --output=.out
#SBATCH --error=.err
#SBATCH --cpus-per-task=48
#SBATCH --ntasks=1
#SBATCH --time=00:60:00
#SBATCH --qos=debug

scontrol show hostnames ${SLURM_JOB_NODELIST} > hfile

sleep 3600

wait

