#!/bin/bash
CFILE=meminfo; [ $# -ge 1 ] && CFILE=$1
LFILE=.log; [ $# -ge 2 ] && LFILE=$2
touch ${LFILE}
vals=( $( cat /proc/meminfo | grep Mem[FT] | tr -s " "  | cut -d" " -f 2 ) )
REFMEM=$(( ${vals[0]} - ${vals[1]} ))
while [ -f "${LFILE}" ]; do
    sleep 0.1
    vals=( $( cat /proc/meminfo | grep Mem[FT] | tr -s " "  | cut -d" " -f 2 ) )
    echo $(( ${vals[0]} - ${vals[1]} - ${REFMEM} )) >> ${CFILE}
done
