#!/bin/bash

PROCS="48 24 12 8 6 4 2 1"
MODS="ompss omp"
PROCS="48"
InputF=in.rhodo.scaled
ExtraF=data.rhodo
IS_INTEL=true
wdir=Memory
[ -d ${wdir} ] && rm -rf ${wdir}
mkdir ${wdir}

bdir=$(realpath ./)
tdir=$(realpath ../)
CMEM=$(realpath ./MemCheck.py)

function do_scrpt( ) {
script=$1
NTHREA=$2
EXE=$3
InpF=$4
mod=$5
[ ${mod} == "omp" ] && EXE="${EXE}  -sf omp -pk omp ${NTHREA}"
EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
cat << EOF > ${script}
#!/bin/bash
export OMP_NUM_THREADS=${NTHREA}
${EXE} -i ${InpF}
EOF
chmod +x ${script}
}

function do_hfile( ) {
    IS_INTEL=false
    mpirun --version | grep -i intel &> /dev/null && IS_INTEL=true
    ${IS_INTEL} && HFILE_FORMAT=":" || HFILE_FORMAT=" slots="
    if [ ! -z "${SLURM_NODELIST}" ]; then
        for line in $( scontrol show hostnames $SLURM_JOB_NODELIST ); do
            echo "$line${HFILE_FORMAT}48"
        done > hfile
    else
        echo "localhost${HFILE_FORMAT}${NPROCS}" > hfile
    fi
}


cd ${wdir}
ScrF=./lammps.sh
for p in ${PROCS}; do
    t=$(( 48/$p ))
    for mod in ${MODS}; do
        wdir=$( printf "${mod}_%02d_%02d" $p $t )
        mkdir $wdir
        pushd $wdir &> /dev/null
        EXE="${tdir}/lmp_"${mod}
        cp ${bdir}/${InputF} .
        for f in ${ExtraF}; do cp ${bdir}/$f .; done
        do_scrpt ${ScrF} $t ${EXE} ${InputF} ${mod}
        do_hfile

        ${CMEM} meminfo .log &
        mpirun -np ${p} --hostfile hfile ${ScrF} 2>&1 | tee output
        rm .log

        popd &> /dev/null
    done
done


echo ADIOS $tdir $bdir

