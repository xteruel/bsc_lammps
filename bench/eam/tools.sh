#!/bin/bash

NPROCS=8
NTHREA=12
SCALES="1 2 6 8"
MODES="MPI OMPSS OMP"

wdir=Traces

input_f="in.eam"
data_f="Cu_u3.eam"
topdir=$(realpath ../../)
script=./lammps.sh
EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4

[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}

src_ompss="#!/bin/bash
export OMP_NUM_THREADS=${NTHREA}
export LD_PRELOAD=${EXTRAE_HOME}/lib/libnanosmpitrace.so
export EXTRAE_CONFIG_FILE=extrae.xml
export NX_ARGS=\"--pes ${NTHREA} --instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state\"
export NX_INSTRUMENTATION=extrae
${topdir}/lmp_ompss_prv -pk ompss -sf ompss -i ${input_f}"

src_omp="#!/bin/bash
export OMP_NUM_THREADS=${NTHREA}
export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
export EXTRAE_CONFIG_FILE=extrae.xml
${topdir}/lmp_omp_prv -sf omp -pk omp ${NTHREA} -i ${input_f}"

src_mpi="#!/bin/bash
unset OMP_NUM_THREADS
export LD_PRELOAD=${EXTRAE_HOME}/lib/libmpitrace.so
export EXTRAE_CONFIG_FILE=extrae.xml
${topdir}/lmp_omp_prv -i ${input_f}"

function makeHostFile( ) {
    NTASK=$1
    TAxNO=$(( NTASK/SLURM_NNODES ))
    NP=$(( SLURM_NNODES*48 ))
    export SLURM_NPROCS="${NTASKS}"
    export SLURM_NTASKS="${NTASKS}"
    [ ${SLURM_NNODES} -eq 1 ] &&
        export SLURM_TASKS_PER_NODE="${NTASKS}" ||
        export SLURM_TASKS_PER_NODE="${TAxNO}(x${SLURM_NNODES})"
    for line in $( scontrol show hostnames ${SLURM_JOB_NODELIST} ); do
        echo "${line}:${TAxNO}"
    done > hfile
}

for SCALE in ${SCALES}; do
    for MODE in ${MODES}; do
        WDIR=$( printf "${wdir}/${MODE}_%02d" ${SCALE} )
        mkdir ${WDIR}
        cp ${input_f} ${data_f} extrae.xml ${WDIR}
        pushd ${WDIR} &> /dev/null

        txt=$(<${input_f})
        echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${input_f}

        [ "${MODE}" == "OMPSS" ] && { src=${src_ompss}; np=${NPROCS}; }
        [ "${MODE}" == "OMP" ] && { src=${src_omp}; np=${NPROCS}; }
        [ "${MODE}" == "MPI" ] && { src=${src_mpi}; np=96; }
        makeHostFile ${np}
        echo -e "${src}" > ${script}
        chmod +x ${script}
        mpirun -machine hfile ${script}
        popd &> /dev/null
    done
done
