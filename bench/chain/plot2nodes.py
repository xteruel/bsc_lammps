#!/usr/bin/env python
from os.path  import isdir, join, isfile, basename
from os       import listdir, getcwd, getenv

import numpy as np
import matplotlib.pyplot as plt

PLOT= getenv("NOPLOT") == None
if PLOT : print( "*** export NOPLOT to suppres plots" )

PNAME = basename( getcwd() ).upper()  # capitalize()

WDIR    = "2Nodes"
st_tags = [ "OMP", "OMPSS", "MPI" ] # "kokkos"

plot_bx = [ 121, 122 ]
ax      = [ None, None ]

SCA=2.5
FIGSIZE = [ SCA*6.4, SCA*4.8 ]
#COLORS  = [ "b", "r", "g" ]
COLORS={ "OMP": "b", "OMPSS": "r", "MPI": "g" }
fntsiz1 = 18
fntsiz2 = 16
fntsiz3 = 12

if not isdir(WDIR) :
    print( "Could not find directory:", WDIR )
    exit()

sdirs= [ d for d in listdir(WDIR) if isdir(join(WDIR,d)) ]
scales={ }
for d in sorted(sdirs) :
    sc = int(d.split("_")[1])
    if sc not in scales :
        scales[sc] = [ d ]
    else :
        scales[sc] = scales[sc] + [ d ]

SHOW=False
OFILE   = "output"
#APP=[ "lj", "cycle" ]
APP=[ "cycle" ]

for t in scales :
    dirs = scales[t]
    natoms=0
    GRAPH={}
    for d in dirs :
        mode, SCA, NP = d.split("_")[:]
        NP=int(NP)
        NT=96/NP
        fn=join(WDIR,d,"output")
        if not isfile(fn) : exit( "Error: missing file '"+fn+"'" )
        lines=open(fn).readlines( )
        if not natoms :
            for l in lines :
                if "Loop time" in l :
                    natoms = int(l.split()[-2] )
                    break
        times=np.array( sorted([ float(l.split("=")[1]) for l in lines if "TIME_cycle" in l ]) )
        ## MEAN of the first half
        avgt=np.mean(times[:times.size//2])
        #avgt=times[0]
        name = "%dp/%dt"%( NP, NT )
        if mode not in GRAPH :
            GRAPH[mode] = { "x": [ name ], "y": [ avgt ] }
        else :
            GRAPH[mode]["x"] = GRAPH[mode]["x"] + [ name ]
            GRAPH[mode]["y"] = GRAPH[mode]["y"] + [ avgt ]
    print( GRAPH )
    fig = plt.figure(figsize=FIGSIZE)
    for mode in GRAPH :
        x = np.array(GRAPH[mode]["x"][::-1])
        y = np.array(GRAPH[mode]["y"][::-1])
        plt.plot( x, y, COLORS[mode], label=mode )
        plt.plot( x, y, COLORS[mode], marker='o' )
        plt.legend()

    plt.ylabel('Time', fontsize=fntsiz2, labelpad=5, fontweight='bold')
    plt.xlabel('Threads per Process', fontsize=fntsiz2, labelpad=5, fontweight='bold')
    plt.xticks(fontsize=fntsiz3)
    plt.yticks(fontsize=fntsiz3)
    fig.suptitle(PNAME+'(%d atoms): Time x Iteration (scaled=%d)'%( natoms, t ), fontsize=fntsiz1, fontweight='bold')

    plt.ylim(bottom=0)
    if PLOT : plt.show()
    of="2nodes_%02d.png"%( t )
    fig.savefig( of, dpi=100 )
