#!/bin/bash
SCALE=5
NPROC=6; [ $# -ge 1 ] && [ "$1" -eq "$1" ] &> /dev/null && [ $1 -gt 0 ] && NPROC=$1
NTHRE=8; [ $# -ge 2 ] && [ "$2" -eq "$2" ] &> /dev/null && [ $2 -gt 0 ] && NTHRE=$2
InputF=in.chain.scaled
INputA="data.chain extrae.xml"
scriptF=./src.sh
txt=$(<${InputF})

bdir=$(realpath ./)
tdir=$(realpath ../../LAMMPS)

wdir=Test
[ -d ${wdir} ] && rm -rf ${wdir}; mkdir ${wdir}; cd ${wdir}

#cp ${bdir}/${InputF} .
echo "${txt}" | sed -e "s/index 1/index ${SCALE}/g" > ${InputF}
for f in ${INputA}; do cp ${bdir}/${f} .; done

EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
if true; then
    EXE=${tdir}/lmp_ompss_prv
    src="#!/bin/bash
    export OMP_NUM_THREADS=${NTHRE}
    export EXTRAE_CONFIG_FILE=extrae.xml
    export NX_INSTRUMENTATION=extrae
    export NX_ARGS=\"--instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state\"
    export LD_PRELOAD=${EXTRAE_HOME}/lib/libnanosmpitrace.so
    ${EXE} -i ${InputF}"
    echo "${src}" > ${scriptF}
    chmod +x ${scriptF}
    mpirun -np ${NPROC} ${scriptF} | tee kk1
    #mpirun -np ${NPROC} ${scriptF} > kk1
    # | tee kk1
fi

if true; then
    EXE=${tdir}/lmp_omp_prv
    src="#!/bin/bash
    export OMP_NUM_THREADS=${NTHRE}
    export EXTRAE_CONFIG_FILE=extrae.xml
    export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
    ${EXE} -sf omp -pk omp ${NTHRE} -i ${InputF}"
#    ${EXE} -i ${InputF}"
    echo "${src}" > ${scriptF}
    chmod +x ${scriptF}
    mpirun -np ${NPROC} ${scriptF} > kk2
    #| tee kk2
fi

if false; then
    EXE=${tdir}/lmp_kokkos
    src="#!/bin/bash
    export OMP_NUM_THREADS=${NTHRE}
    ${EXE} -sf kk -k on t ${NTHRE} -i ${InputF}"
    echo "${src}" > ${scriptF}
    chmod +x ${scriptF}
    mpirun -np ${NPROC} ${scriptF} > kk3
fi
