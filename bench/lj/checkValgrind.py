#!/usr/bin/env python
from __future__ import print_function
from sys      import argv
from os.path  import isdir, join, isfile

EXE=argv.pop(0)
FILES= argv if argv else [ "Valgrind/kk1" ]

for f in FILES :
    if not isfile(f) :
        print( "Could't open file", f )
        exit(1)
    txt=open(f).readlines( )
    print( "*********  Checking file", f )
    n=len(txt)
    i = 0
    while i< n and "== HEAP SUMMARY:" not in txt[i] : i +=1
    i+=4
    DESC = {}
    for d in [ "MPI", "NANOS", "THREAD" ] :
        DESC[d] = [ 0, 0 ]
    while i<n and "blocks" in txt[i]  :
        j = i+1
        while j<n and ":" in txt[j] : j += 1
        if j == i+1 : print( "CHECK this" )
        block =  "".join(txt[i:j+1])
        mem = int(txt[i].split()[1].replace(',',''))
        if "MPI_Init" in block or "MPI_Cart_create" in block : tag = "MPI"
        elif "nanos_submit" in block : tag = "NANOS"
        elif "start_thread" : tag = "THREAD"
        else :
            print( "What to do with block:"  )
            print( block )
            exit(0)
        DESC[tag] = [ DESC[tag][0]+1, DESC[tag][1]+mem ]
        i = j+1
    if "LEAK SUMMARY:" not in txt[i] :
        print( "Check this line" )
        print( txt[i], end='' )
        exit(0)
    for k,v in DESC.items( ) :
        print( "*********  %10s"%( k ), v )
    print( "".join(txt[i+1:i+5]) )
