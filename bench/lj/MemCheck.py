#!/usr/bin/env python
from sys     import argv
from os.path import isfile
from time    import sleep
from psutil  import virtual_memory

def GetAvailableMem( ) :
#    f=open("/proc/meminfo")
#    mTo=int(f.readline().split()[1])
#    mAv=int(f.readline().split()[1])
#    f.close()
#    return mTo-mAv
    return virtual_memory( ).available

CFILE="meminfo" if len(argv) < 2 else argv[1]
LFILE=".log" if len(argv) < 3 else argv[2]
open(LFILE,'a').close()
refMem = GetAvailableMem( )

ofile = open(CFILE,"w",1)
while isfile(LFILE) :
    sleep( 0.01 )
    nMem = GetAvailableMem( )
    ofile.write( "%d\n"%( (refMem-nMem)/1024 ) )
ofile.close( )
