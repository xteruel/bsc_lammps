#!/usr/bin/env python
from os.path  import isdir, join, isfile, basename
from os       import listdir, getcwd

import numpy as np

PNAME = basename( getcwd() ).capitalize()

WDIR    = "ScanSize"
st_tags = [ "omp", "ompss", "kokkos" ]
OFILE   = "output"

MEM_INFO=1
MEM_OUTP=2
MODE=MEM_OUTP
SHOW=False

SCA=2.5
FIGSIZE = [ SCA*6.4, SCA*4.8 ]
if not isdir(WDIR) : exit()

sdirs= [ d for d in listdir(WDIR) if isdir(join(WDIR,d)) ]

for i in range(8) :
    COL    = [ ]
    natoms = None
    for t in st_tags :
        COL.append( sorted([ d for d in sdirs if d.startswith( "%d_%s_"%( i+1, t ) ) ]) )
    n=len(COL[0])
    AX=[]
    for j in range(n) :
        MSG = None
        yy = [ ]
        tt = [ ]
        for it, C in enumerate(COL) :
            f=join(WDIR,C[j],OFILE)
            if not natoms :
                for l in open(f,"r").readlines( ) :
                    if l.startswith( "Loop time" ) :
                        natoms=l.split()[-2]
                        break
                print( PNAME+'('+natoms+' atoms) : Memory use (kb) x Iteration (scaled=%d)'%( i+1 ) )
            if not MSG :
                pr, th = [ int(a) for a in C[j].split("_")[-2:] ]
                MSG="%2d Procs %2d Threads"%( pr, th )
                print( )
                print( MSG )
                lines=[]
            y=[ a.split()[-1] for a in open(f).readlines() if a.startswith( "MEMORY            ITER" ) ]
            yy += [ y ]
            t = [ a.split("=")[1].split()[0] for a in open(f).readlines() if "TIME_cycle=" in a ]
            tt += [ t ]
        yy_ = [ list(ind) for ind in zip(*yy) ]
        tt_ = [ list(ind) for ind in zip(*tt) ]
        imax = max(len(yy_),len(tt_))
        
        print( "\t".join( ["Iterations"]+st_tags+st_tags ) )
        for k in range(imax) :
            iy = it = k
            if k>= len(yy_) : iy = len(yy_)-1
            if k>= len(tt_) : it = len(tt_)-1
            print( "\t".join( [str(k+1)]+yy_[iy]+tt_[it] ) )
            
