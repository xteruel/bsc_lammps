/* ---------------------------------------------
Makefile constructed configuration:
mar oct  8 10:10:32 CEST 2019
----------------------------------------------*/
#if !defined(KOKKOS_MACROS_HPP) || defined(KOKKOS_CORE_CONFIG_H)
#error "Do not include KokkosCore_config.h directly; include Kokkos_Macros.hpp instead."
#else
#define KOKKOS_CORE_CONFIG_H
#endif
/* Execution Spaces */
#define KOKKOS_HAVE_OPENMP
/* General Settings */
#define KOKKOS_HAVE_CXX11
#define KOKKOS_ENABLE_PROFILING
#define KOKKOS_ENABLE_DEPRECATED_CODE
/* Optimization Settings */
/* Cuda Settings */
