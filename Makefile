
ompSRCDIR=./omp
ompDESTDIR=./.omp$(EXT)

omp2SRCDIR=./ompss
omp2DESTDIR=./.omp2$(EXT)

ompssSRCDIR=./ompss
ompssDESTDIR=./.ompss_$(OMPSS_VERS)$(EXT)

kokkosSRCDIR=./kokkos
kokkosDESTDIR=./.kokkos$(EXT)

include arch.make

$(shell mkdir -p $(ompDESTDIR) )
$(shell mkdir -p $(omp2DESTDIR) )
$(shell mkdir -p $(ompssDESTDIR) )
$(shell mkdir -p $(kokkosDESTDIR) )

ompSRCS=$(wildcard $(ompSRCDIR)/*.cpp)
ompOBJS=$(patsubst $(ompSRCDIR)/%.cpp,$(ompDESTDIR)/%.o,$(ompSRCS))

omp2SRCS=$(wildcard $(omp2SRCDIR)/*.cpp)
omp2OBJS=$(patsubst $(omp2SRCDIR)/%.cpp,$(omp2DESTDIR)/%.o,$(omp2SRCS))

ompssSRCS=$(wildcard $(ompssSRCDIR)/*.cpp)
ompssOBJS=$(patsubst $(ompssSRCDIR)/%.cpp,$(ompssDESTDIR)/%.o,$(ompssSRCS))

kokkosSRCS=$(wildcard $(kokkosSRCDIR)/*.cpp)
kokkosOBJS=$(patsubst $(kokkosSRCDIR)/%.cpp,$(kokkosDESTDIR)/%.o,$(kokkosSRCS))

instr=verlet comm_brick \
      fix_nve fix_nve_omp \
      npair_half_bin_atomonly_newton npair_half_bin_atomonly_newton_omp

      
instrOBJS=$(patsubst %,$(ompDESTDIR)/%.o,$(instr))


ompssSpe= \
    accelerator_ompss \
    angle_charmm_ompss \
    atom \
    bond_fene_ompss \
    bond_harmonic_ompss \
    comm_brick_ompss \
    compute_erotate_sphere \
    compute_temp \
    create_atoms \
    dihedral_charmm_ompss \
    domain \
    fft3d \
    fix_freeze \
    fix_gravity \
    fix_langevin_ompss \
    fix_neigh_history_ompss \
    fix_nh \
    fix_nve_ompss \
    fix_nve_sphere \
    fix_shake_ompss \
    group \
    improper_harmonic_ompss \
    kspace \
    modify \
    nbin_standard \
    neighbor \
    npair_half_bin_atomonly_newton_ompss \
    npair_half_bin_newton_ompss \
    npair_half_size_bin_newtoff_ompss \
    ntopo_angle_partial_ompss \
    ntopo_bond_partial_ompss \
    ntopo_dihedral_all_ompss \
    ntopo_improper_all_ompss \
    pair_eam_ompss \
    pair_gran_hooke_history_ompss \
    pair_lj_charmm_coul_long_ompss \
    pair_lj_cut_ompss \
    pppm_ompss \
    remap \ \
    replicate \
    thr_ompss \
    velocity \
    verlet

ompssSpeObj=$(patsubst %,$(ompssDESTDIR)/%.o,$(ompssSpe))
ompSpe=finish input kissfft comm
ompSpeObj=$(patsubst %,$(ompssDESTDIR)/%.o,$(ompSpe))

OMP_EXE=lmp_omp$(EXT)
OMP2_EXE=lmp_omp_2$(EXT)
OMPSS_EXE=lmp_ompss_$(OMPSS_VERS)$(EXT)
KOKKOS_EXE=lmp_kokkos$(EXT)

.PHONY: directories

#bin: directories $(OMP_EXE) $(OMP2_EXE)
bin: directories $(OMP_EXE) $(OMP2_EXE) $(OMPSS_EXE) $(KOKKOS_EXE)

$(ompOBJS):   DEFINES += -DLMP_USER_OMP
$(ompssOBJS): DEFINES += ${OMPSS_DEFINES}
$(instrOBJS): CXXFLAGS += $(INSTRU)
$(kokkosOBJS): DEFINES += -DLMP_KOKKOS -DLMP_USER_OMP
$(kokkosOBJS): CXXFLAGS += -std=c++11

$(OMP_EXE): $(ompOBJS)
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

$(ompDESTDIR)/%.o: $(ompSRCDIR)/%.cpp
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) $(DEFINES) -c -o $@ $<

$(OMP2_EXE): $(omp2OBJS)
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

$(omp2DESTDIR)/%.o: $(omp2SRCDIR)/%.cpp
	$(CXX) $(OMP_FLAGS) $(EFLAGS) $(CXXFLAGS) $(DEFINES) -c -o $@ $<

$(OMPSS_EXE): $(ompssOBJS)
	$(MCXX) $(OMPSS_FLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)
	#${PFLAGS} $(CXX) $(OMPSS_FLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

#$(ompssSpeObj): CXX := $(PFLAGS) $(CXX)
$(ompssSpeObj): CXX := $(MCXX)
$(ompssSpeObj): CXXFLAGS += $(OMPSS_FLAGS)
$(ompSpeObj):   CXXFLAGS += $(OMP_FLAGS)

$(ompssDESTDIR)/pair_comb.o: MCXX := $(CXX)

$(ompssDESTDIR)/%.o: $(ompssSRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(EFLAGS) $(DEFINES) -c -o $@ $<

$(KOKKOS_EXE): $(kokkosOBJS)
	$(CXX) $(KOKKOS_FLAGS) $(EFLAGS) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

$(kokkosDESTDIR)/%.o: $(kokkosSRCDIR)/%.cpp
	@echo HOLA 
	$(CXX) $(KOKKOS_FLAGS) $(EFLAGS) $(CXXFLAGS) $(DEFINES) -c -o $@ $<


clean:
	rm -rf $(ompDESTDIR) $(OMP_EXE)
	rm -rf $(ompssDESTDIR) $(OMPSS_EXE)
	rm -rf $(kokkosDESTDIR) $(KOKKOS_EXE)
	rm -rf mcxx_*.cpp


verlet.o: verlet.h
style_integrate.h: verlet.h
update.o: style_integrate.h
lampss.o: style_integrate.h

-include .dep
